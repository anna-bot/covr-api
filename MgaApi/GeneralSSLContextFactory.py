#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Generate SSL contex factory"""
import os
from OpenSSL import SSL
from twisted.internet import ssl


class GeneralSSLContextFactory(object):
    """This class generates a ssl context factory"""

    def generateSSLContextFactory(self, sslKey, sslCrt):
        """ generates ssl context factory object
        @sslKey: path to ssl private key
        @sslCrt: path to ssl cert (pem) file
        @return: ssl context factory
        """

        sslmethod = 'SSLv23_METHOD'
        sslmethod = getattr(SSL, sslmethod, SSL.SSLv23_METHOD)

        directoryPath = os.path.dirname(os.path.abspath(__file__))

        try:
            # getting ssl key file
            sslKeyContent = open(sslKey).read()
        except Exception, e:
            # error in getting ssl key file
            excMsg = str(e)
            print 'Can not read content in ssl key file: ', excMsg
            try:
                # reading content in fallback ssl
                keyFallbackFile = directoryPath+'/Certs/privkey.pem'
                sslKeyContent = open(keyFallbackFile).read()
                sslKey = keyFallbackFile
                print 'Using fallback ssl key file from: ', keyFallbackFile
            except Exception, e:
                excMsg = str(e)
                print 'Can not read content in fallback ssl key file: ', excMsg

        try:
            # getting ssl cert file
            sslCrtContent = open(sslCrt).read()
        except Exception, e:
            # error in getting ssl cert file
            excMsg = str(e)
            print 'Can not read content in ssl cert file: ', excMsg
            try:
                # reading content in fallback ssl crt file
                crtFallbackFile = directoryPath+'/Certs/cert.pem'
                sslCrtContent = open(crtFallbackFile).read()
                sslCrt = crtFallbackFile
                print 'Using fallback ssl crt file from: ', crtFallbackFile
            except Exception, e:
                excMsg = str(e)
                print 'Can not read content in fallback ssl crt file: ', excMsg

        print 'using ssl key file:', sslKey
        print 'using ssl cert file:', sslCrt

        if sslCrt.endswith('.pem') and 'chain' in sslCrt:
            # Once twisted version 14 or greater is installed, we can use this to enable curve ciphers
            # for pem do cat key crt > pem
            try:
                certData = open(sslCrt).read()
            except Exception, e:
                excMsg = str(e)
                print 'ERROR: %s' % (excMsg)
            certificate = ssl.PrivateCertificate.loadPEM(certData)
            sslContextFactory = certificate.options()
        else:
            sslContextFactory = ssl.DefaultOpenSSLContextFactory(sslKey, sslCrt, sslmethod=sslmethod)
        sslContext = sslContextFactory.getContext()
        sslContext.set_cipher_list('HIGH:MID:!CAMELLIA256-SHA:!CAMELLIA128-SHA')
        sslContext.set_options(SSL.OP_NO_SSLv2 | SSL.OP_NO_SSLv3 | getattr(ssl.SSL, 'OP_NO_COMPRESSION', 0x00020000) |
            getattr(ssl.SSL, 'OP_CIPHER_SERVER_PREFERENCE ', 0x00400000))

        return sslContextFactory


# if __name__ == "__main__":
#     generalSSLContextFactory = GeneralSSLContextFactory()
#     privKey = '/etc/letsencrypt/live/api.covomo.com/privkey.pem'
#     crt = '/etc/letsencrypt/live/api.covomo.com/cert.pem'
# 
#     resultGenerateSSLContextfactory = generalSSLContextFactory.generateSSLContextFactory(privKey, crt)
#     print 'resultGenerateSSLContextfactory:', resultGenerateSSLContextfactory
