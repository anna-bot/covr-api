#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Handler Insurance Contract"""
import types
import logging
from Validator import GeneralValidator
from JsonFileHandler import JsonFileHandler


class ServiceInsuranceContract(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceInsuranceContract.__init__")
        self.jsonFileHandler = JsonFileHandler()
        self.generalValidator = GeneralValidator()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceInsuranceContract.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        """ get an contract by belonging_id
            parameters needed:
            - belonging_id
        """
        self.LOG.info("called ServiceInsuranceContract.get")
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        parameterBelongingId = requestData.get('requestArgs', {}).get('filter', {}).get('belonging_id', '')

        if not parameterBelongingId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Belonging id missing in request'
            return responseService

        # search based on belonging_id
        insuranceContractsFromFile = self.jsonFileHandler.getData('dbInsuranceContracts.json')
        insuranceContractsForGivenBelongingId = list(filter(lambda d: d.get('belonging_id', '') == parameterBelongingId, insuranceContractsFromFile))
        if not insuranceContractsForGivenBelongingId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Belonging id not existing'
            return responseService

        responseService['response'] = {'insurance_contract': insuranceContractsForGivenBelongingId}
        return responseService

    def serviceCancel(self, requestData):
        """ cancels a contract on insurer side and remove it from contracts
            parameters needed:
            - contract_id
        """
        self.LOG.info("called ServiceInsuranceContract.cancel")
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        parameterContractId = requestData.get('requestArgs', {}).get('filter', {}).get('contract_id', '')

        if not parameterContractId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Contract id missing in request'
            return responseService

        # look if contract is existing
        insuranceContractsFromFile = self.jsonFileHandler.getData('dbInsuranceContracts.json')
        self.LOG.info('****** len insuranceContractsFromFile: %s' % (str(len(insuranceContractsFromFile))))
        changedInsuranceContracts = list(filter(lambda d: d['id'] != parameterContractId, insuranceContractsFromFile))
        self.LOG.info('****** len changedInsuranceContracts: %s' % (str(len(changedInsuranceContracts))))

        if len(insuranceContractsFromFile) == len(changedInsuranceContracts):
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Contract id not found'
            return responseService

        # if contract was found, call external service to cancel contract
        # assume on current stage, that cancel was successful
        # delete contract from contract database (later maybe marked only as deleted for history)

        # store changed insurance contract data
        self.jsonFileHandler.storeData('dbInsuranceContracts.json', changedInsuranceContracts )
        responseService['message'] = 'Contract cancelled'
        return responseService

    def serviceUpdate(self, requestData):
        """ updates a value of the contract data
            parameter needed:
            - contract_id
            (and value for update)
        """
        self.LOG.info("called ServiceInsuranceContract.update")
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        parameterContractIdToUpdate = requestData.get('requestArgs', {}).get('items', {}).get('contract_id', '')
        parameterContractAutomaticRenewal = requestData.get('requestArgs', {}).get('items', {}).get('content', {}).get('automatic_renewal', '')

        if not parameterContractIdToUpdate:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Contract id missing in request'
            return responseService

        # validate request content for value 'automatic_renewal'
        validatedAutomaticRenewal = self.generalValidator.buildBooleanValue(parameterContractAutomaticRenewal)

        if not isinstance(validatedAutomaticRenewal, types.BooleanType):
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Value of automatic_renewal is invalid'
            return responseService

        # get contract information
        insuranceContractsFromFile = self.jsonFileHandler.getData('dbInsuranceContracts.json')
        contractForGivenContractId = list(filter(lambda d: d['id'] == parameterContractIdToUpdate, insuranceContractsFromFile))
        # if not found throw error
        if not contractForGivenContractId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Contract id not found'
            return responseService

        contractData = contractForGivenContractId[0]

        # change content to updated values
        contractData['automatic_renewal'] = validatedAutomaticRenewal

        # renove original contract (for flat file only, on database it will be an update command only)
        changedInsuranceContractFromFile = list(filter(lambda d: d['id'] != parameterContractIdToUpdate, insuranceContractsFromFile))
        # add changed content again
        changedInsuranceContractFromFile.append(contractData)

        # store store changed insuranceContract
        self.jsonFileHandler.storeData('dbInsuranceContracts.json', changedInsuranceContractFromFile)
        return responseService
