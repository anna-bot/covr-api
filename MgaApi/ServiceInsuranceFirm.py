#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging


class ServiceInsuranceFirm(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceInsuranceFirm.__init__")
        print "called ServiceInsuranceFirm.__init__"

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceInsuranceFirm.handleService")
        print "called ServiceInsuranceFirm.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceInsuranceFirm.get")
        print "called ServiceInsuranceFirm.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        insuranceFirm = [
            {'id': 'bd158ba580', 'name': 'ERGO Direkt Versicherung AG'},
            {'id': 'fc12f6c045', 'name': 'Würzburger'},
            {'id': 'e28d4842bb', 'name': 'Assona'}
            ]

        responseService['response'] = {'insurance_firm': insuranceFirm}

        return responseService
