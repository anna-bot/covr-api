#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging
import random
from datetime import datetime
from dateutil.relativedelta import relativedelta


from uuid import uuid4
from copy import copy
from JsonFileHandler import JsonFileHandler


class ServiceInsuranceOffer(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceInsuranceOffer.__init__")
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceInsuranceOffer.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        """ builds an insurance offer on external insurance service
            parameters needed:
            - belonging_id
            - purchase_price
        """
        self.LOG.info("called ServiceInsuranceOffer.get")
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        parameterBelongingId = requestData.get('requestArgs', {}).get('filter', {}).get('belonging_id', '')
        parameterPurchasePrice = requestData.get('requestArgs', {}).get('filter', {}).get('purchase_price', 0.0)

        if not parameterBelongingId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Belonging id missing in request'
            return responseService

        if not parameterPurchasePrice:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Purchase price missing in request'
            return responseService

        if parameterPurchasePrice:
            parameterPurchasePrice = float(parameterPurchasePrice)


        # search if belongiong exist (and later will use data from belonging)
        belongingOverviewFromFile = self.jsonFileHandler.getData('dbBelongingOverview.json')
        belongingsForGivenId = list(filter(lambda d: d['id'] == parameterBelongingId, belongingOverviewFromFile))
        if not belongingsForGivenId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Belonging id not existing'
            return responseService


        # build fake offer content

        fakeOffer = {
            'id': uuid4().get_hex()[:10],
            'name': 'Basisschutz TF-1',
            'belonging_id': parameterBelongingId,
            'insurer': {'id': 'f58a78b547', 'name_key': 'allianz', 'name': 'Allianz', 'logo_image': 'assets/logos/allianz.png', 'lang': 'de-DE'},
            'broker': {'id': '8456c18a50', 'name_key': 'assona', 'name': 'assona GmbH', 'logo_image': 'assets/logos/assona.gif', 'lang': 'de-DE'},
            'insured_value': parameterPurchasePrice,  # 'PURCHASE_PRICE',
            'deductible_value': 0.00,  # round(float(10.0*parameterPurchasePrice/100.0), 2),  # '10% VON PURCHASE PRICE'
            'offer': [
                {'premium': round(float(0.15*parameterPurchasePrice/100.0), 2),  # 0.15% VON PUCHASE PRICE,
                 'billing_cycle': {'id': '783cc02502', 'name_key': 'daily', 'name': 'Täglich', 'abbreviation': 'Tag', 'lang': 'de-DE'}
                 },
                {'premium': round(float(1.7*parameterPurchasePrice/100.0), 2),  # 1.7% VON PUCHASE PRICE
                 'billing_cycle': {'id': '3937e370f0', 'name_key': 'monthly', 'name': 'Monatlich', 'abbreviation': 'Mo.', 'lang': 'de-DE'}
                 }
                # {'premium': round(float(7.0*parameterPurchasePrice/100.0), 2),  # 7% VON PUCHASE PRICE
                #  'billing_cycle': {'id': 'ag64h8d84e', 'name_key': 'yearly', 'name': 'Jährlich', 'abbreviation': 'Jahr', 'lang': 'de-DE'}
                #  }
                ],
            'terms': [
                {'id': 'fe28138d3c', 'name': 'Sturz- und Bruchschäden', 'lang': 'de-DE'},
                {'id': '3651eefc76', 'name': 'Wasserschäden', 'lang': 'de-DE'},
                {'id': '6fc15e22d4', 'name': 'Bedienungsfehler', 'lang': 'de-DE'},
                {'id': 'a1daeb2372', 'name': 'Diebstahl', 'lang': 'de-DE'}
                ],
            'details': 'Extra info',
            'info_links': [
                {'key': 'AVB', 'value': 'https://www.covomo.de/media/download/insurer/t_168_avb.pdf'},
                {'key': 'Produktbroschüre', 'value': 'https://www.covomo.de/media/download/insurer/t_168_broc.pdf'}
                ]
            }

        # get offers from file
        insuranceOfferFromFile = self.jsonFileHandler.getData('dbInsuranceOffers.json')

        addInsuranceOffer = copy(insuranceOfferFromFile)
        addInsuranceOffer.append(fakeOffer)

        # store insurance offers
        self.jsonFileHandler.storeData('dbInsuranceOffers.json', addInsuranceOffer)

        responseService['response'] = {'insurance_offer': [fakeOffer]}
        return responseService

    def serviceClose(self, requestData):
        """close the offer
            parameters needed:
            - offer_id
            - billing_cycle_id
        """
        self.LOG.info("called ServiceInsuranceOffer.serviceClose")
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        # get insurance offer data based on offer_id

        parameterOfferId = requestData.get('requestArgs', {}).get('filter', {}).get('offer_id', '')
        parameterBillingCycleId = requestData.get('requestArgs', {}).get('filter', {}).get('billing_cycle_id', '')

        if not parameterOfferId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Offer id missing in request'
            return responseService

        if not parameterBillingCycleId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Billing cycle id missing in request'
            return responseService

        # get offer data
        insuranceOfferFromFile = self.jsonFileHandler.getData('dbInsuranceOffers.json')

        offerForGivenId = list(filter(lambda d: d['id'] == parameterOfferId, insuranceOfferFromFile))
        if not offerForGivenId:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Offer id not existing'
            return responseService

        offerData = offerForGivenId[0]

        # check for billing_cycle
        offerBilling = list(filter(lambda d: d.get('billing_cycle', {}).get('id', '') == parameterBillingCycleId, offerData.get('offer', [])))
        if not offerBilling:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Offer billing cycle not existing'
            return responseService

        # insurance data from insurance provider
        contractReference = str(random.randint(1000000, 5000000))
        contractStartDate = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        contractExpirationDate = ''

        # calculate expiration absed on billing_cycle_id
        if parameterBillingCycleId == '783cc02502':
            # daily
            contractExpirationDate = (datetime.today() + relativedelta(days=+1)).strftime('%Y-%m-%d %H:%M:%S')
        elif parameterBillingCycleId == '3937e370f0':
            # monthly
            contractExpirationDate = (datetime.today() + relativedelta(months=+1)).strftime('%Y-%m-%d %H:%M:%S')
        elif parameterBillingCycleId == 'ag64h8d84e':
            # yearly
            contractExpirationDate = (datetime.today() + relativedelta(years=+1)).strftime('%Y-%m-%d %H:%M:%S')
        automaticRenewal = True

        # create closed offer content, by copy main part of offer
        offerClosedContent = copy(offerData)
        offerClosedContent['offer_id'] = offerClosedContent['id']  # store original offer_id for later use maybe
        # add contract_id
        offerClosedContent['id'] = uuid4().get_hex()[:10]
        # replace billing cycles by the cycle from request
        offerClosedContent['offer'] = offerBilling
        # add contract details
        offerClosedContent['contract_reference'] = dict(key='Nr.', value=contractReference)
        offerClosedContent['start_date'] = contractStartDate
        offerClosedContent['expiration_date'] = contractExpirationDate
        offerClosedContent['automatic_renewal'] = automaticRenewal

        # get insurance contract from file
        insuranceContractFromFile = self.jsonFileHandler.getData('dbInsuranceContracts.json')
        addInsuranceContract = copy(insuranceContractFromFile)
        addInsuranceContract.append(offerClosedContent)
        # store insurance offers
        self.jsonFileHandler.storeData('dbInsuranceContracts.json', addInsuranceContract)
        responseService['response'] = {'insurance_offer_close': [offerClosedContent]}
        return responseService
