#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service: Belonging Overview"""

import random
import logging
import types
from uuid import uuid4
from copy import copy
from Validator import GeneralValidator
from JsonFileHandler import JsonFileHandler


class ServiceBelongingOverview(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceBelongingOverview.__init__")
        self.jsonFileHandler = JsonFileHandler()
        self.generalValidator = GeneralValidator()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceBelongingOverview.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceBelongingOverview.get")
        # self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        # belongingsOverviewItems = [
        #     {
        #         'id': '4ed1da869f',
        #         'manufacturer': {'id': 'j23h3', 'name': 'Canon', 'logo_image': 'assets/logos/canon.jpg', 'lang': 'de-DE' },
        #         'name': 'CANON EOS 700D Spiegelreflexkamera',
        #         'category': {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #         'purchase_price': 800.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 10.00,
        #         'image': 'assets/images/canon.png'},
        #     {
        #         'id': '6c1e554a6a',
        #         'manufacturer': {'id': 'hg23jh', 'name': 'Samsung', 'logo_image': 'assets/logos/samsung.jpg', 'lang': 'de-DE' },
        #         'name': 'SAMSUNG Galaxy S6 edge',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 1200.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 17.55,
        #         'image': 'assets/images/galaxy.png'},
        #     {
        #         'id': '0cd49c08af',
        #         'manufacturer': {'id': 'jujhg2ra', 'name': 'JURA', 'logo_image': 'assets/logos/jura.png', 'lang': 'de-DE' },
        #         'name': 'JURA 15129 Z6, Kaffeevollautomat',
        #         'category': {'id': '857ffc6da1', 'parent_id': '', 'name': 'Haushalt', 'icon': 'home'},
        #         'purchase_price': 2000.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 12.13,
        #         'image': 'assets/images/jura.png'},
        #     {
        #         'id': '163a40d137',
        #         'manufacturer': {'id': 'juikpms', 'name': 'Microsoft', 'logo_image': 'assets/logos/microsoft.jpg', 'lang': 'de-DE' },
        #         'name': 'MICROSOFT Surface Pro 4',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 1200.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 5.50,
        #         'image': 'assets/images/surface.png'},
        #     {
        #         'id': '57129c5005',
        #         'manufacturer': {'id': 'jh2g3', 'name': 'Sony', 'logo_image': 'assets/logos/sony.svg', 'lang': 'de-DE' },
        #         'name': 'SONY SEL-70200GM 70 mm-200 mm f/2.8',
        #         'category': {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #         'purchase_price': 2999.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 5.90,
        #         'image': 'assets/images/objective.png'},
        #     {
        #         'id': '90a426f1e0','manufacturer': {'id': '62fhv2', 'name': 'Apple', 'logo_image': 'assets/logos/apple.png', 'lang': 'de-DE' },
        #         'name': 'APPLE iPad Pro WiFi + Cellular, Tablet mit 12.9 Zoll, 256 GB Speicher, iOS 9, Silber',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 1269.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 10.45,
        #         'image': 'assets/images/ipad.png'},
        #     {
        #         'id': '97f40fb633',
        #         'manufacturer': {'id': 'jh3lkj32', 'name': 'LG', 'logo_image': 'assets/logos/lg.jpg', 'lang': 'de-DE' },
        #         'name': 'LG 75UH780V, 190 cm (75 Zoll), UHD 4K',
        #         'category': {'id': '857ffc6da1', 'parent_id': '', 'name': 'Haushalt', 'icon': 'home'},
        #         'purchase_price': 4499.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 30.10,
        #         'image': 'assets/images/lg-tv.png'},
        #     {
        #         'id': 'd2f91c3ec9',
        #         'manufacturer': {'id': '62fhv2', 'name': 'Apple', 'logo_image': 'assets/logos/apple.png', 'lang': 'de-DE' },
        #         'name': 'MacBook Pro mit Touch Bar und Touch ID, Notebook mit 15.4 Zoll Display',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 3199.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 7.90,
        #         'image': 'assets/images/macbook.png'},
        #     {
        #         'id': '5b95f8e3cf',
        #         'manufacturer': {'id': 'j23h3', 'name': 'Canon', 'logo_image': 'assets/logos/canon.jpg', 'lang': 'de-DE' },
        #         'name': 'EF 8-15mm f/4L Fisheye USM 8 mm-15 mm Objektiv f/4',
        #         'category': {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #         'purchase_price': 1229.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 15.50,
        #         'image': 'assets/images/objective-fisheye.png'},
        #     {
        #         'id': '10ebf35d98',
        #         'manufacturer': {'id': '43mjhb', 'name': 'Gibson', 'logo_image': 'assets/logos/gibson.png', 'lang': 'de-DE' },
        #         'name': 'Custom Shop Solid Formed 17 Hollowbody Venetian - E-Gitarre',
        #         'category': {'id': 'd8cd2a2a8e', 'parent_id': '', 'name': 'Musik', 'icon': 'music'},
        #         'purchase_price': 6099.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 5.99,
        #         'image': 'assets/images/egitarre.jpg'}
        # ]

        # self.jsonFileHandler.storeData('dbBelongingOverview.json', belongingsOverviewItems)

        belongingOverviewFromFile = self.jsonFileHandler.getData('dbBelongingOverview.json')

        responseService['response'] = {'belonging': belongingOverviewFromFile}
        return responseService

    def serviceDelete(self, requestData):
        self.LOG.info("called ServiceBelongingOverview.delete")

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        idToDelete = requestData.get('requestArgs', {}).get('items', {}).get('id', '')
        if not idToDelete:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Belonging id missing in request'
            return responseService

        # from insert: 80078a5cbe, 3d3cca934b

        # get belogingOverview data
        belongingOverviewFromFile = self.jsonFileHandler.getData('dbBelongingOverview.json')
        # serch for belonging id and remove
        self.LOG.info('****** len belongingOverviewFromFile: %s' % (str(len(belongingOverviewFromFile))))

        changedBelongingOverview = list(filter(lambda d: d['id'] != idToDelete, belongingOverviewFromFile))
        self.LOG.info('****** len changedBelongingOverview: %s' % (str(len(changedBelongingOverview))))
        self.LOG.info(changedBelongingOverview)

        if len(belongingOverviewFromFile) == len(changedBelongingOverview):
            responseService['status'] = 'ERROR'
            responseService['message'] = 'No changes made'
            return responseService

        # store changed belogingOverview data
        self.jsonFileHandler.storeData('dbBelongingOverview.json', changedBelongingOverview )
        # responseService['response'] = {'belonging': belongingOverviewFromFile}
        return responseService

    def serviceUpdate(self, requestData):
        """at current stage will remove element and add the given one from content"""
        self.LOG.info("called ServiceBelongingOverview.update")

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        idToUpdate = requestData.get('requestArgs', {}).get('items', {}).get('id', '')
        if not idToUpdate:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Belonging id missing in request'
            return responseService

        # ### remove old data with id
        # get belogingOverview data
        belongingOverviewFromFile = self.jsonFileHandler.getData('dbBelongingOverview.json')
        # serch for belonging id and remove
        self.LOG.info('****** len belongingOverviewFromFile: %s' % (str(len(belongingOverviewFromFile))))

        changedBelongingOverview = list(filter(lambda d: d['id'] != idToUpdate, belongingOverviewFromFile))
        self.LOG.info('****** len changedBelongingOverview: %s' % (str(len(changedBelongingOverview))))
        self.LOG.info(changedBelongingOverview)

        if len(belongingOverviewFromFile) == len(changedBelongingOverview):
            responseService['status'] = 'ERROR'
            responseService['message'] = 'No changes made'
            return responseService

        # ### add complete given data
        dataToAdd = requestData.get('requestArgs', {}).get('items', {}).get('content', {})
        if not dataToAdd:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Content missing in request'
            return responseService

        # validate some fields
        try:
            dataToAdd['purchase_price'] = float(dataToAdd['purchase_price'])
        except:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Value of purchase_price is invalid'
            return responseService

        validatedIsInsured = self.generalValidator.buildBooleanValue(dataToAdd.get('is_insured', ''))
        if not isinstance(validatedIsInsured, types.BooleanType):
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Value of is_insured is invalid'
            return responseService
        dataToAdd['is_insured'] = validatedIsInsured


        # add new data
        changedBelongingOverview.append(dataToAdd)

        # store store changed belogingOverview
        self.jsonFileHandler.storeData('dbBelongingOverview.json', changedBelongingOverview)
        return responseService

    def serviceAdd(self, requestData):
        self.LOG.info("called ServiceBelongingOverview.add")

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        # fake new belongingOverview id from database
        belongingOverviewId = uuid4().get_hex()[:10]
        # fake lowest_insurance_offer
        lowestInsuranceOffer = round(random.uniform(0.5, 3.0), 2)

        dataToAdd = requestData.get('requestArgs', {}).get('items', {}).get('content', {})
        if not dataToAdd:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Content missing in request'
            return responseService

        # add id, lowest_inrurance_offer; validate some fields
        dataToAdd['id'] = belongingOverviewId
        dataToAdd['lowest_insurance_offer'] = lowestInsuranceOffer
        # try:
        #     dataToAdd['is_insured'] = bool(dataToAdd['is_insured'])
        # except:
        #     pass
        try:
            dataToAdd['purchase_price'] = float(dataToAdd['purchase_price'])
        except:
            responseService['status'] = 'ERROR'
            responseService['message'] = 'Value of purchase_price is invalid'
            return responseService

        # get belogingOverview data
        belongingOverviewFromFile = self.jsonFileHandler.getData('dbBelongingOverview.json')

        addBelongingOverview = copy(belongingOverviewFromFile)
        addBelongingOverview.append(dataToAdd)

        # store store changed belogingOverview

        self.jsonFileHandler.storeData('dbBelongingOverview.json', addBelongingOverview)

        # return lowest_insurance_offer and insuranceOverview id
        newEntryData = {"id": belongingOverviewId, "lowest_insurance_offer": lowestInsuranceOffer}

        responseService['response'] = {'belonging': newEntryData}
        return responseService
