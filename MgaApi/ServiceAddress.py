#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging


class ServiceAddress(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceAddress.__init__")
        print "called ServiceAddress.__init__"

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceAddress.handleService")
        print "called ServiceAddress.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceAddress.get")
        print "called ServiceAddress.get"
        self.LOG.info(requestData)
        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        address = [
            {'id': 'z8dg53eg72',
             'client_id': 'e3c71eb22caf4dfe7f04704779a725f683f615129331ffd22a0c208084817c3d',
             'country_code_id': 'DE',
             'state_code_id': 'DE-BE',
             'zip_code': '10117',
             'location_name': 'Berlin',
             'street': 'Friedrichstr.',
             'house_number': '54',
             'other_references': ''
            }
        ]
        responseService['response'] = {'address': address}
        return responseService
