#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Address Country"""
import logging
from JsonFileHandler import JsonFileHandler


class ServiceAddressCountry(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceAddressCountry.__init__")
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceAddressCountry.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceAddressCountry.get")
        self.LOG.info(requestData)
        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        # dbCountry = [
        #     {'code': 'DE', 'name': 'Deutschland', 'lang': 'de-DE'},
        #     {'code': 'AT', 'name': 'Österreich', 'lang': 'de-DE'},
        #     {'code': 'CH', 'name': 'Schweiz', 'lang': 'de-DE'},
        # ]

        # self.jsonFileHandler.storeData('dbAddressCountry.json', dbCountry)
        countryFromFile = self.jsonFileHandler.getData('dbAddressCountry.json')

        countryResponse = []
        for countryEntry in countryFromFile:
            countryResponse.append(dict(code=countryEntry['code'], name=countryEntry['name']))

        responseService['response'] = {'address_country': countryResponse}
        return responseService
