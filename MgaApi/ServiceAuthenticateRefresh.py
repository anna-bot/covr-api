#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Handler Authenticate Refresh"""
import logging
import mx.DateTime
from Validator import GeneralValidator


class ServiceAuthenticateRefresh(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceAuthenticateRefresh.__init__")
        print "called ServiceAuthenticateRefresh.__init__"
        self.generalValidator = GeneralValidator()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceAuthenticateRefresh.handleService")
        print "called ServiceAuthenticateRefresh.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceRefresh(self, requestData):
        self.LOG.info("called ServiceAuthenticateRefresh.serviceRefresh")
        print "called ServiceAuthenticateRefresh.serviceRefresh"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        authCustomerDataRequest = requestData.get('requestArgs', {}).get('auth', {})
        self.LOG.info("******** auth customer data request ********")
        self.LOG.info(authCustomerDataRequest)
        self.LOG.info("**********************************************************")

        # authIdentifier = authCustomerDataRequest.get('identifier', '')
        authSecurityToken = authCustomerDataRequest.get('security_token', '')

        # check for valid identifier and security token
        # if not self.generalValidator.isAuthIdentifier(authIdentifier):
        #     responseService = {'status': 'ERROR', 'message': 'Invalid identifier', 'code': ''}
        #     return responseService
        if not self.generalValidator.isAuthSecurityToken(authSecurityToken):
            responseService = {'status': 'ERROR', 'message': 'Invalid security token', 'code': ''}
            return responseService

        # set fake response
        # validate identifier and security token
        if authSecurityToken != '2629ea0967e84075a2580b0ebdbf859f':
            responseService = {'status': 'ERROR', 'message': 'Authentication failed', 'code': ''}
            return responseService

        # return new token or new exire date
        authResponse = {
            # 'identifier': 'e3c71eb22caf4dfe7f04704779a725f683f615129331ffd22a0c208084817c3d',
            'security_token': '2629ea0967e84075a2580b0ebdbf859f',
            'expire': self.createTokenExpireTime()
            }
        responseService['response'] = {'auth': authResponse}
        return responseService

    def createTokenExpireTime(self):
        """creates timestamp when token expires"""
        maxTokenAge = mx.DateTime.oneHour * 1  # 1 hour
        return (mx.DateTime.now()+maxTokenAge).strftime('%Y-%m-%dT%H:%M:%S')
