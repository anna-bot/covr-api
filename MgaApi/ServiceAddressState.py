#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Address State"""
import logging
from JsonFileHandler import JsonFileHandler


class ServiceAddressState(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceAddressState.__init__")
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceAddressState.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceAddressState.get")
        self.LOG.info(requestData)
        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        # dbState = [
        #     {'code': 'DE-BW', 'name': 'Baden-Württemberg', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-BY', 'name': 'Bayern', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-BE', 'name': 'Berlin', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-BB', 'name': 'Brandenburg', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-HB', 'name': 'Bremen', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-HH', 'name': 'Hamburg', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-HE', 'name': 'Hessen', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-MV', 'name': 'Mecklenburg-Vorpommern', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-NI', 'name': 'Niedersachsen', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-NW', 'name': 'Nordrhein-Westfalen', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-RP', 'name': 'Rheinland-Pfalz', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-SL', 'name': 'Saarland', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-SN', 'name': 'Sachsen', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-ST', 'name': 'Sachsen-Anhalt', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-SH', 'name': 'Schleswig-Holstein', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'DE-TH', 'name': 'Thüringen', 'lang': 'de-DE', 'country_code': 'DE'},
        #     {'code': 'AT-1', 'name': 'Burgenland', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-2', 'name': 'Kärnten', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-3', 'name': 'Niederösterreich', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-4', 'name': 'Oberösterreich', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-5', 'name': 'Salzburg', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-6', 'name': 'Steiermark', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-7', 'name': 'Tirol', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-8', 'name': 'Vorarlberg', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'AT-9', 'name': 'Wien', 'lang': 'de-DE', 'country_code': 'AT'},
        #     {'code': 'CH-AG', 'name': 'Aargau', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-AR', 'name': 'Appenzell Ausserrhoden', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-AI', 'name': 'Appenzell Innerrhoden', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-BL', 'name': 'Basel-Landschaft', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-BS', 'name': 'Basel-Stadt', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-BE', 'name': 'Bern', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-FR', 'name': 'Freiburg', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-GE', 'name': 'Genève', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-GL', 'name': 'Glarus', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-GR', 'name': 'Graubünden', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-JU', 'name': 'Jura', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-LU', 'name': 'Luzern', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-NE', 'name': 'Neuchâtel', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-NW', 'name': 'Nidwalden', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-OW', 'name': 'Obwalden', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-SG', 'name': 'Sankt Gallen', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-SH', 'name': 'Schaffhausen', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-SZ', 'name': 'Schwyz', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-SO', 'name': 'Solothurn', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-TG', 'name': 'Thurgau', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-TI', 'name': 'Ticino', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-UR', 'name': 'Uri', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-VS', 'name': 'Wallis', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-VD', 'name': 'Vaud', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-ZG', 'name': 'Zug', 'lang': 'de-DE', 'country_code': 'CH'},
        #     {'code': 'CH-ZH', 'name': 'Zürich', 'lang': 'de-DE', 'country_code': 'CH'},
        # ]

        # self.jsonFileHandler.storeData('dbAddressState.json', dbState)

        # check for filter and apply filter
        filterCountryId = requestData.get('requestArgs', {}).get('filter', {}).get('country_id', '')

        stateFromFile = self.jsonFileHandler.getData('dbAddressState.json')
        stateResponse = []
        for countryEntry in stateFromFile:
            if filterCountryId:
                if countryEntry.get('country_code', '').upper() == filterCountryId.upper():
                    stateResponse.append(dict(code=countryEntry['code'], name=countryEntry['name']))
            else:
                stateResponse.append(dict(code=countryEntry['code'], name=countryEntry['name']))
        responseService['response'] = {'address_state': stateResponse}
        return responseService
