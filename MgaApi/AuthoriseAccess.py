#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Class to authorise access"""
import logging
import base64


class AuthoriseAccess(object):
    """Class to authorise access"""
    def __init__(self):
        self.LOG = logging.getLogger(__name__)

    def authoriseFromRequestHeader(self, requestObject, grantedAccess=[]):
        """ get Data from request object and checks authorisation
        @requestObject: request object
        @return: dictionary with status like: {'status': 'ERROR', 'message': '', 'code': ''} or {'status': 'SUCCESS', 'message': 'Access granted', 'code': ''}
        """

        self.LOG = logging.getLogger(__name__)
        authorisationFromHeader = requestObject.getHeader('Authorization')
        authFromRequestResponse = {'status': 'ERROR', 'message': 'Authorisation failed', 'code': ''}

        if not authorisationFromHeader:
            authFromRequestResponse = {'status': 'ERROR', 'message': 'No authorization data in request', 'code': ''}
            self.LOG.error(authFromRequestResponse)
        else:
            auth = authorisationFromHeader.replace("Basic ", "")
            hashCode = base64.decodestring(auth)
            self.LOG.info('Get Basic Authorization (decoded): %s' % (str(hashCode)))
            # right now check via granted access parameter in options
            # use check in access api later
            if hashCode in grantedAccess:
                authFromRequestResponse = {'status': 'SUCCESS', 'message': 'Access granted', 'code': ''}
            else:
                self.LOG.info('Authorization failed with key: %s' % (str(hashCode)))
        return authFromRequestResponse
