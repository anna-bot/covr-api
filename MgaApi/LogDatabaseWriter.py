#! /usr/bin/python
# -*- coding: utf-8; -*-
import json
import logging
import types
from mx import DateTime
from twisted.internet import defer


class LogDatabaseWriter(object):
    def __init__(self, dbDict, *args, **kwargs):
        self.LOG = logging.getLogger(__name__)
        self.LOG.debug('called LogDatabaseWriter.__init__')
        self.logDatabase = dbDict['logsDb']

    def storeHansemerkurData(self, response, requestData):
        self.LOG.debug('called LogDatabaseWriter.storeHansemerkurData')
        isError = False
        message = ''
        jsonDumpsResponse = json.dumps(response)
        if isinstance(response, types.DictType) and response.get('status', '').upper() == 'ERROR':
            isError = True
            # remove single quote in json response -> results in sql error
            try:
                jsonDumpsResponse = jsonDumpsResponse.replace("'", "")
            except:
                pass
        try:
            message = response.get('message', '')
            message = message.replace("'", "")
        except:
            pass

        # dynamic tablename for log based on current day
        dynamicLogTable = 'hansemerkur_logs_%s' % (DateTime.now().strftime('%Y-%m-%d'))
        query = """INSERT into "%s" (request, response, ip, section, hash, is_error, policy_number, method, message, api_session) VALUES ('%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s')""" % (dynamicLogTable,  json.dumps(requestData.get('requestArgs','')), jsonDumpsResponse, requestData.get('requestOriginalClientIP',''), requestData.get('requestPathSection',''), requestData.get('hash', ''), isError, response.get('policy_number', ''), requestData.get('method', ''), message, requestData.get('api_session',''))
        d = self.logDatabase.runInteraction(self._executeEnterDB, query)
        d.addCallback(self.logCallback)
        return d

    def _executeEnterDB(self, cursor, query):
        try:
            self.LOG.info(query)
            cursor.execute(query)
            return
        except Exception, e:
            excMsg = str(e)
            self.LOG.error("******************************************************************")
            self.LOG.error("LogDatabaseWriter - <exception> - %s" % (excMsg))
            self.LOG.error("SQL: %s" % (query))
            self.LOG.error("******************************************************************")
            return

    def logCallback(self, response):
        return 1