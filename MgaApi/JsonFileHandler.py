#! /usr/bin/python
# -*- coding: utf-8; -*-
import json
import logging


class JsonFileHandler(object):
    """read/write json data and returns python dict"""

    def __init__(self):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called JsonFileHandler.__init__")
        self.filePath = "/home/mgaapi/mga-api/MgaApi/JsonDb/"  # live server
        # self.filePath = "/home/mgaapi/JsonDb/"  # development

    def getData(self, fileName):
        """ read data from a json file"""

        with open(self.filePath+fileName) as jsonFileIn:
            data = json.load(jsonFileIn)
        return data

    def storeData(self, fileName, data):
        """store data into json file"""
        with open(self.filePath+fileName, 'w') as jsonFileOut:
            json.dump(data, jsonFileOut)
