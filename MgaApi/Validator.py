#! /usr/bin/python
# -*- coding: utf-8; -*-
import logging
import types
import re


class GeneralValidator(object):

    def __init__(self):
        self.LOG = logging.getLogger(__name__)
        self.LOG.debug('called GeneralValidator.__init__')
        self.IS_USERNAME_PATTERN = re.compile('^[a-z]{10,}$', re.IGNORECASE)
        self.IS_PASSWORD_PATTERN = re.compile('^[a-z0-9]{7,}$', re.IGNORECASE)
        self.IS_EMAIL_PATTERN = re.compile('[^@]+\@[^@]+?\.[a-zA-Z]+',re.IGNORECASE)
        self.IS_AUTH_IDENTIFIER_PATTERN = re.compile('^[a-z0-9]{10,}$', re.IGNORECASE)
        self.IS_AUTH_SECURITY_TOKEN_PATTERN = re.compile('^[a-z0-9]{10,}$', re.IGNORECASE)

    def checkStringWithPattern(self, string='', pattern=None):
        if not isinstance(string, types.StringTypes):
            return False

        if pattern is not None:
            if pattern.findall(string):
                return True
            else:
                return False
        else:
            return False

    def isEmail(self, source):
        """ checks if hash is valid
        @source (str): 'user@host.com'
        @return (bool): True for success, False otherwise
        """
        if not isinstance(source, types.StringTypes):
            return False
        return self.checkStringWithPattern(string=source, pattern=self.IS_EMAIL_PATTERN)

    def isUsername(self, source):
        """ checks if username is valid
        @source (str): 'test'
        @return (bool): True for success, False otherwise
        """
        if not isinstance(source, types.StringTypes):
            return False
        # at current state check for length only
        if len(source.strip()) < 6:
            return False
        else:
            return True
        # return self.checkStringWithPattern(string=source, pattern=self.IS_USERNAME_PATTERN)

    def isPassword(self, source):
        """ checks if password is valid
        @source (str): 'test123'
        @return (bool): True for success, False otherwise
        """
        if not isinstance(source, types.StringTypes):
            return False
        if len(source.strip()) < 6:
            return False
        else:
            return True
        # return self.checkStringWithPattern(string=source, pattern=self.IS_PASSWORD_PATTERN)

    def isAuthIdentifier(self, source):
        """ checks if auth identifier is valid
        @source (str): 'a6d6db3474rbnf93'
        @return (bool): True for success, False otherwise
        """
        if not isinstance(source, types.StringTypes):
            return False
        return self.checkStringWithPattern(string=source, pattern=self.IS_AUTH_IDENTIFIER_PATTERN)

    def isAuthSecurityToken(self, source):
        """ checks if auth security token is valid
        @source (str): 'a6d6db3474rbnf93'
        @return (bool): True for success, False otherwise
        """
        if not isinstance(source, types.StringTypes):
            return False
        return self.checkStringWithPattern(string=source, pattern=self.IS_AUTH_SECURITY_TOKEN_PATTERN)

    def buildBooleanValue(self, source):
        """ tries to return real boolean values of strings.
            if type is boolean already, return input value.
        @souce (str, bool): 'True'
        @return (bool or None): True
        """
        dataOut = None
        if isinstance(source, types.BooleanType):
            dataOut = source
        elif isinstance(source, types.StringTypes):
            # try to match correct values for booelan True
            if source.upper() in ['TRUE', 'T', '1']:
                dataOut = True
            elif source.upper() in ['FALSE', 'F', '0']:
                dataOut = False
        return dataOut
