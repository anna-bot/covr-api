#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging
import mx.DateTime
# import datetime
from twisted.internet import reactor
from twisted.internet import defer
# from Validator import GeneralValidator
# from SessionData import SessionData
from LogDatabaseWriter import LogDatabaseWriter


from ServiceAuthenticateRegister import ServiceAuthenticateRegister
from ServiceAuthenticateLogin import ServiceAuthenticateLogin
from ServiceAuthenticateRefresh import ServiceAuthenticateRefresh
from ServiceBelonging import ServiceBelonging
from ServiceBelongingOverview import ServiceBelongingOverview
from ServiceBelongingSummary import ServiceBelongingSummary
from ServiceCategory import ServiceCategory
from ServiceInsuranceContract import ServiceInsuranceContract
from ServiceInsuranceCoverage import ServiceInsuranceCoverage
from ServiceInsuranceFirm import ServiceInsuranceFirm
from ServiceInsuranceOffer import ServiceInsuranceOffer
from ServiceInsuranceOfferOverview import ServiceInsuranceOfferOverview
from ServiceAccount import ServiceAccount
from ServiceProduct import ServiceProduct
from ServiceManufacturer import ServiceManufacturer
from ServiceAddress import ServiceAddress
from ServiceAddressCountry import ServiceAddressCountry
from ServiceAddressState import ServiceAddressState


class MgaApiCore(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.debug('called MgaApiCore.__init__')
        # self.generalValidator = GeneralValidator()
        # self.sessionData = SessionData(dbDict)
        self.logDatabaseWriter = LogDatabaseWriter(dbDict)

        self.serviceAuthenticateRegister = ServiceAuthenticateRegister(dbDict)
        self.serviceAuthenticateLogin = ServiceAuthenticateLogin(dbDict)
        self.serviceAuthenticateRefresh = ServiceAuthenticateRefresh(dbDict)
        self.serviceBelonging = ServiceBelonging(dbDict)
        self.serviceBelongingOverview = ServiceBelongingOverview(dbDict)
        self.serviceBelongingSummary = ServiceBelongingSummary(dbDict)
        self.serviceCategory = ServiceCategory(dbDict)
        self.serviceInsuranceContract = ServiceInsuranceContract(dbDict)
        self.serviceInsuranceCoverage = ServiceInsuranceCoverage(dbDict)
        self.serviceInsuranceFirm = ServiceInsuranceFirm(dbDict)
        self.serviceInsuranceOffer = ServiceInsuranceOffer(dbDict)
        self.serviceInsuranceOfferOverview = ServiceInsuranceOfferOverview(dbDict)
        self.serviceProduct = ServiceProduct(dbDict)
        self.serviceManufacturer = ServiceManufacturer(dbDict)
        self.serviceAccount = ServiceAccount(dbDict)
        self.serviceAddress = ServiceAddress(dbDict)
        self.serviceAddressCountry = ServiceAddressCountry(dbDict)
        self.serviceAddressState = ServiceAddressState(dbDict)

    def marshallServiceAuthenticateRegister(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAuthenticateRegister')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAuthenticateRegister.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAuthenticateRegister')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceAuthenticateLogin(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAuthenticateLogin')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAuthenticateLogin.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAuthenticateLogin')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceAuthenticateRefresh(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAuthenticateRefresh')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAuthenticateRefresh.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAuthenticateRefresh')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceBelonging(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceBelonging')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceBelonging.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceBelonging')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceBelongingOverview(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceBelongingOverview')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceBelongingOverview.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceBelongingOverview')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceBelongingSummary(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceBelongingSummary')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceBelongingSummary.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceBelongingSummary')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceCategory(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceCategory')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceCategory.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceCategory')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceInsuranceContract(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceInsuranceContract')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceInsuranceContract.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceInsuranceContract')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceInsuranceCoverage(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceInsuranceCoverage')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceInsuranceCoverage.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceInsuranceCoverage')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceInsuranceFirm(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceInsuranceFirm')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceInsuranceFirm.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceInsuranceFirm')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceInsuranceOffer(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceInsuranceOffer')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceInsuranceOffer.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceInsuranceOffer')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceInsuranceOfferOverview(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceInsuranceOfferOverview')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceInsuranceOfferOverview.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceInsuranceOfferOverview')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceProduct(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceProduct')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceProduct.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceProduct')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d


    def marshallServiceManufacturer(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceManufacturer')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceManufacturer.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceManufacturer')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d


    def marshallServiceAccount(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAccount')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAccount.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAccount')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceAddress(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAddress')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAddress.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAddress')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceAddressCountry(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAddressCountry')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAddressCountry.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAddressCountry')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def marshallServiceAddressState(self, serviceMethodMain, serviceMethodSub, requestData):
        self.LOG.debug('called MgaApiCore.marshallServiceAddressState')
        # self.LOG.info('Received requestData: %s' % (requestData))
        serviceParameters = {'serviceMethodMain': serviceMethodMain, 'serviceMethodSub': serviceMethodSub, 'requestData': requestData}
        d = defer.Deferred()
        # d.addCallback(self.printData, 'Request')
        d.addCallback(self.serviceAddressState.handleService)
        d.addErrback(self._errorHandler)
        d.addBoth(self.printData, 'Response')
        d.addBoth(self.printTiming, mx.DateTime.now().ticks(), 'marshallServiceAddressState')
        # d.addBoth(self.postprocessBackground, requestData)
        d.callback(serviceParameters)
        return d

    def printData(self, data, comment=''):
        self.LOG.info('------------------ %s ------------------' % (comment))
        self.LOG.info(data)
        self.LOG.info('----------------------------------------')
        return data

    def _errorHandler(self, failure):
        self.LOG.debug('called MgaApiCore._errorHandler')
        failureTypeName = str(failure.type.__name__)
        failureGetErrorMessage = str(failure.getErrorMessage())

        self.LOG.error("------------------ PROCESSING ERROR ------------------")
        self.LOG.error("Failuretype     : %s" % (failureTypeName))
        self.LOG.error("FailureMessage  : %s" % (failureGetErrorMessage))

        self.LOG.error("FailureTraceback: %s" % (str(failure.getTraceback())))
        self.LOG.error("------------------------------------------------------")
        errorMessage = '%s' % (failureGetErrorMessage)
        return {'status': 'ERROR', 'message': errorMessage, 'code': ''}

    def printTiming(self, result, start, method=''):
        # self.LOG.debug('called printTiming')
        self.LOG.info("TIMING_%s: %0.4f" % (str(method), mx.DateTime.now().ticks()-start))
        return result

    def postprocessBackground(self, response, requestData):
        self.LOG.debug('called MgaApiCore.postprocessBackground')
        d = defer.Deferred()
        d.addCallback(self.logDatabaseWriter.storeSessionData, requestData)
        reactor.callWhenRunning(d.callback, response)
        return response
