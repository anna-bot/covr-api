from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from subprocess import Popen, PIPE, STDOUT
import os
from types import *
import smtplib


class Mail:
    """eMail sending class"""

    def __init__(self, sendmailPath='', smtp='', smtpUser='', smtpPassword='', smtpStarttls=False):
        """sendmailPath defaults to /usr/sbin/sendmail. if installation differs, give it to constructor
           if smtp is provided this smtp server will be used
        """
        self.SMTP = None
        self.SENDMAIL = '/usr/sbin/sendmail'

        if sendmailPath:
            self.SENDMAIL = sendmailPath

        # smtp = "localhost:25"
        if smtp:
            self.SMTP = dict(server=smtp)
            if smtpUser and smtpPassword:
                self.SMTP['user'] = smtpUser
                self.SMTP['pass'] = smtpPassword
                self.SMTP['starttls'] = smtpStarttls

        self.lastMessage = {}

    def sendmailHtml(self, to, fro, subject, htmlbody, cc='', bcc='', attachments=None, pictures=None,  replyto=''):
        body = []
        try:
            from BeautifulSoup import BeautifulSoup
            soup = BeautifulSoup(htmlbody)
            # print "OrigEncoding: ", soup.originEncoding
            htmlbody = str(soup)
        except Exception, e:
            print "Unable to Beautify html with BeautifulSoup: %s" % e

        # take care of text encoding
        if isinstance(htmlbody, UnicodeType):
            htmltext = htmlbody.encode("ascii", 'xmlcharrefreplace')
        elif isinstance(htmlbody, StringType):
            try:
                unicodebody = unicode(htmlbody, 'utf-8')
                htmltext = unicodebody.encode("ascii", 'xmlcharrefreplace')
            except Exception, e:
                print "Unable to encode MailMessage: %s" % e
                return False
        else:
            print "Unsupported type for htmlbody: %s " % type(htmlbody)
            return False

        try:
            # ## append Multipart with html and plain text alternative
            msg = MIMEMultipart('alternative')

            # try to convert it to text und put it in as alternative too
            try:
                from html2text import html2text

                html2text.IGNOR_IMAGES = True
                html2text.BODY_WIDTH = 0
                html2text.UNICODE_SNOB = 1

                text = html2text(htmltext)
                text = text.replace('&nbsp_place_holder;', ' ')
                msg.attach(MIMEText(text, 'plain'))
            except Exception, e:
                print "Unable to convert html to text for mailing (please install html2text package): %s" % e

            msg.attach(MIMEText(htmltext, 'html'))
            body.append(msg)

        except Exception, e:
            print "Unable to build MultipartMail: %s" % e

            # append html text only
            body.append(MIMEText(htmlbody, 'html'))

        # add pictures
        related = False
        for key, picfile in pictures and pictures.items() or []:
            if 'cid:%s' % key in htmltext:
                related = True
                fp = open(picfile, 'rb')
                filename = picfile.split('/')[-1]
                msgImage = MIMEImage(fp.read())
                fp.close()
                msgImage.set_param('name', filename)
                msgImage.add_header('Content-ID', '<%s>' % key)
                msgImage.add_header('Content-Disposition', 'attachment', filename=filename)
                body.append(msgImage)

        # check for additional attachments
        body.extend(self._addAttachments(attachments))

        return self.sendmailMultiPart(to, fro, subject, body, cc, bcc, isrelated=related, replyto=replyto)

    def _addAttachments(self, attachments):
        """
        Send email with attachments. attachments are array of two
        elements - StringIO and a title, it can contain an additional mime type also
        """
        if not attachments:
            return []

        result = []
        maintype = 'application'
        subtype = 'octet-stream'

        for item in attachments:
            buffer, attach_name = item[:2]
            if len(item) > 3:
                maintype, subtype = item[2:5]
            attach = MIMEBase(maintype, subtype)
            attach.set_payload(buffer.read())
            encoders.encode_base64(attach)
            attach.add_header('Content-Disposition', 'attachment', filename=attach_name)
            result.append(attach)
        return result

    def sendmailMultiPart(self, to, fro, subject, body, cc='', bcc='', isrelated=False, replyto=''):
        if not isinstance(body, ListType):  # send as plain mail
            return self.sendmail(to, fro, subject, body, cc, bcc)

        if isrelated:
            msg = MIMEMultipart('related')  # check
        else:
            msg = MIMEMultipart()  # check

        msg['Subject'] = subject
        msg['From'] = fro
        msg['To'] = to
        msg['Bcc'] = bcc
        msg['Cc'] = cc
        if replyto:
            msg['Reply-To'] = replyto

        for be in body:
            msg.attach(be)

        # fh = open('/tmp/test.mail', 'w')
        # fh.write(msg.as_string())
        # fh.close()

        return self._sendMail(msg.as_string(), fro, to)

    def sendmail(self, to, fro, subject, body, cc='', bcc='', overwrite=None, replyto=''):
        """method for sending mail via sendmail pipe"""

        if overwrite:
            msg = MIMEText('%s\n%s' % (overwrite, body))
        else:
            msg = MIMEText("""%s""" % body)
            msg['Subject'] = subject
            msg['From'] = fro
            msg['To'] = to
            msg['Bcc'] = bcc
            msg['Cc'] = cc
            if replyto:
                msg['Reply-To'] = replyto

        return self._sendMail(msg.as_string(), fro, to)

    def getLastMessageText(self):
        return self.lastMessage.get('raw', '')

    def _sendMail(self, mssg, fro, to):
        self.lastMessage = {'from': fro, 'to': to, 'raw': mssg}

        if self.SMTP:
            server = None
            try:
                server = smtplib.SMTP(self.SMTP['server'])
                if self.SMTP.get('starttls'):
                    server.starttls()
                if self.SMTP.get('user'):
                    server.login(self.SMTP['user'], self.SMTP['pass'])
                res = server.sendmail(fro, to, mssg)
            except Exception, e:
                res = '%s (config: %s)' % (e, self.SMTP)
            finally:
                if server:
                    server.quit()

            if res:
                print "smtp error: %s" % res
                return 'smtp error: %s' % res
            else:
                return True
        else:
            p = Popen([self.SENDMAIL, "-t", "-f", "%s" % fro],
                      stdout=PIPE, stdin=PIPE, stderr=STDOUT)
            grep_stdout = p.communicate(input=mssg)[0]
            exitcode = p.returncode
            if exitcode:
                print "sendmail exit code: %s, %s" % (exitcode, grep_stdout)
                return "sendmail exit code: %s, %s" % (exitcode, grep_stdout)
            return True

    def smtpmail(self):
        print "dummy"
