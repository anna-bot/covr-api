#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging


class ServiceInsuranceOfferOverview(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceInsuranceOfferOverview.__init__")
        print "called ServiceInsuranceOfferOverview.__init__"

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceInsuranceOfferOverview.handleService")
        print "called ServiceInsuranceOfferOverview.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceInsuranceOfferOverview.get")
        print "called ServiceInsuranceOfferOverview.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        insurancesOffer = [
            {'id': 'b94ab12938', 'belonging_id': '4ed1da869f', 'insured_value': 800.00, 'deductible_value': 100.00, 'premium': 5.99, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Diebstahl']},
            {'id': '15dc3b9d4d', 'belonging_id': '6c1e554a6a', 'insured_value': 1200.00, 'deductible_value': 200.00, 'premium': 20.59, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': '9c7da36954', 'belonging_id': '0cd49c08af', 'insured_value': 2000.00, 'deductible_value': 100.00, 'premium': 15.99, 'coverage': ['Bedienungsfehler', 'Schäden durch Flüssigkeit', 'Diebstahl']},
            {'id': '9ec49faa8c', 'belonging_id': '163a40d137', 'insured_value': 600.00, 'deductible_value': 80.00, 'premium': 20.00, 'coverage': ['Bedienungsfehler', 'Schäden durch Flüssigkeit', 'Diebstahl']},
            {'id': 'ebd86975cf', 'belonging_id': '4d4adaceec', 'insured_value': 3600.00, 'deductible_value': 70.00, 'premium': 30.00, 'coverage': ['Diebstahl', 'Sturz- und Bruchschäden']},
            {'id': 'c2253c19c2', 'belonging_id': '57129c5005', 'insured_value': 2999.00, 'deductible_value': 180.00, 'premium': 20.59, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': 'cad13e3e7f', 'belonging_id': '90a426f1e0', 'insured_value': 1269.00, 'deductible_value': 100.00, 'premium': 10.99, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': '6d696e8052', 'belonging_id': '97f40fb633', 'insured_value': 4499.00, 'deductible_value': 450.00, 'premium': 35.59, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': '0814a602f3', 'belonging_id': 'd2f91c3ec9', 'insured_value': 3199.00, 'deductible_value': 230.00, 'premium': 27.80, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': 'db7f2a3c38', 'belonging_id': '5b95f8e3cf', 'insured_value': 1229.00, 'deductible_value': 150.00, 'premium': 10.90, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': '0a58a74c94', 'belonging_id': '10ebf35d98', 'insured_value': 6099.00, 'deductible_value': 500.00, 'premium': 30.90, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': '619ea2f53c', 'belonging_id': 'e84ec443d3', 'insured_value': 3949.00, 'deductible_value': 350.00, 'premium': 25.99, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']},
            {'id': '980a919530', 'belonging_id': '2390e0a09e', 'insured_value': 6990.00, 'deductible_value': 370.00, 'premium': 23.30, 'coverage': ['Sturz- und Bruchschäden', 'Wasserschäden', 'Bedienungsfehler', 'Kurzschluss und Überspannung', 'Material- und Konstruktionsfehler', 'Diebstahl']}
            ]
        responseService['response'] = {'insurance_offer_overview': insurancesOffer}
        return responseService
