#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service: Account"""
import logging


class ServiceAccount(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceAccount.__init__")
        print "called ServiceAccount.__init__"

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceAccount.handleService")
        print "called ServiceAccount.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceAccount.get")
        print "called ServiceAccount.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        account = [
            {'title': '',
             'salutation': 'mr.',
             'first_name': 'Henrique',
             'surname': 'Schulz',
             'birth_date': '1991-01-18',
             'birth_place': 'Brasilien',
             'marital_status': 'single',
             'phone_number': '+4901570123456',
             'email': 'henrique.schulz@covomo.de',
             'image': 'https://media.licdn.com/media/p/3/005/068/32d/229cadf.jpg',
             'address': [{'id': 'z8dg53eg72',
                            'client_id': 'e3c71eb22caf4dfe7f04704779a725f683f615129331ffd22a0c208084817c3d',
                            'country_code_id': 'DE',
                            'state_code_id': 'DE-BE',
                            'type': 'home',
                            'zip_code': '10117',
                            'location_name': 'Berlin',
                            'street': 'Friedrichstr.',
                            'house_number': '54',
                            'other_references': ''}
                            ]
             }
            ]
        responseService['response'] = {'account': account}
        return responseService
