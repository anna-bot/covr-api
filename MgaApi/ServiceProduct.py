#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Product"""
import logging
from JsonFileHandler import JsonFileHandler


class ServiceProduct(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceProduct.__init__")
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceProduct.handleService")
        print "called ServiceProduct.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceProduct.get")
        print "called ServiceProduct.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        # products = [
        #     {'id': 'f0f0678e3d', 'name': 'iPhone 7 Smartphone (11,9 cm (4,7 Zoll), 128GB', 'category_id': 'c78dcb36dc', 'manufacturer_id': '62fhv2', 'image': 'assets/images/iphoneblack.jpg'},
        #     {'id': '0535ddd062', 'name': 'iPhone 5S Smartphone 16GB (10,2 cm (4 Zoll)', 'category_id': 'c78dcb36dc', 'manufacturer_id': '62fhv2', 'image': 'assets/images/iphone5s.jpg'},
        #     {'id': '90842b45ca', 'name': 'MacBook Pro Late 2016 Silver 256 GB', 'category_id': 'ed2aaa52b4', 'manufacturer_id': '62fhv2', 'image': 'assets/images/mac256.jpg'},
        #     {'id': '479ec0fa30', 'name': 'MacBook Air 33,78 cm (13,3 Zoll)', 'category_id': 'ed2aaa52b4', 'manufacturer_id': '62fhv2', 'image': 'assets/images/macair.jpg'},
        #     {'id': 'd1899a8609', 'name': 'Galaxy S6 Smartphone (5,1 Zoll) 32 GB Speicher', 'category_id': 'c78dcb36dc', 'manufacturer_id': 'hg23jh', 'image': 'assets/images/galaxys6.jpg'},
        #     {'id': '323cfd5ebc', 'name': 'Galaxy S5 Smartphone (5,1 Zoll) 16 GB Speicher', 'category_id': 'c78dcb36dc', 'manufacturer_id': 'hg23jh', 'image': 'assets/images/galaxys5.jpg'},
        #     {'id': '389ed7405e', 'name': 'Notebook (Intel Core i5 3317U, 1,7GHz, 8GB RAM, 500GB HDD)', 'category_id': 'ed2aaa52b4', 'manufacturer_id': 'hg23jh', 'image': 'assets/images/samsunglaptop.jpg'},
        #     {'id': '9c2a0c122a', 'name': 'G6 Smartphone (5,7 Zoll) 32GB Speicher', 'category_id': 'c78dcb36dc', 'manufacturer_id': 'jh3lkj32', 'image': 'assets/images/lgg6.jpg'},
        #     {'id': '1ce58e9080', 'name': 'Notebook, 14 Zoll Full-HD, Intel Core i5-6200U, 4 GB RAM, 256 GB SSD', 'category_id': 'ed2aaa52b4', 'manufacturer_id': 'jh3lkj32', 'image': 'assets/images/lglaptop.jpg'},
        #     {'id': '165dbb9c8a', 'name': 'Notebook VAIO (13,1 Zoll) Intel Core TM i7-3612QM, 2.10 Ghz Prozessor, 8 GB SDRAM, 256 GB SSD', 'category_id': 'ed2aaa52b4', 'manufacturer_id': 'jh2g3', 'image': 'assets/images/sonyvaio.jpg'},
        #     {'id': 'd361a163f3', 'name': 'VAIO Duo SVD1121X9EB 29,4 cm (11,6 Zoll Touch) Convertible Ultrabook', 'category_id': 'ed2aaa52b4', 'manufacturer_id': 'jh2g3', 'image': 'assets/images/sonyultra.jpg'},
        #     {'id': 'c789aba5ec', 'name': 'EOS 700D Spiegelreflexkamera', 'category_id': 'cb68c91a41', 'manufacturer_id': 'j23h3', 'image': 'assets/images/canon.png'},
        #     {'id': '3f6a6a5ca8', 'name': 'SEL-70200GM 70 mm-200 mm f/2.8', 'category_id': 'cb68c91a41', 'manufacturer_id': 'jh2g3', 'image': 'assets/images/objective.png'},
        #     {'id': '0edf89090f', 'name': 'EF 8-15mm f/4L Fisheye USM 8 mm-15 mm Objektiv f/4', 'category_id': 'cb68c91a41', 'manufacturer_id': 'j23h3', 'image': 'assets/images/objective-fisheye.png'},
        #     {'id': 'cda0f967cc', 'name': 'Galaxy S6 edge', 'category_id': 'ed2aaa52b4', 'manufacturer_id': 'hg23jh', 'image': 'assets/images/galaxy.png'},
        #     {'id': '2638b0ef9a', 'name': 'Surface Pro 4', 'category_id': 'ed2aaa52b4', 'manufacturer_id': 'juikpms', 'image': 'assets/images/surface.png'},
        #     {'id': 'd83919ffa5', 'name': 'iPad Pro WiFi + Cellular, Tablet mit 12.9 Zoll, 256 GB Speicher, iOS 9, Silber', 'category_id': 'ed2aaa52b4', 'manufacturer_id': '62fhv2', 'image':  'assets/images/ipad.png'},
        #     {'id': 'c434413af4', 'name': 'MacBook Pro mit Touch Bar und Touch ID, Notebook mit 15.4 Zoll Display', 'category_id': 'ed2aaa52b4', 'manufacturer_id': '62fhv2', 'image': 'assets/images/macbook.png'},
        #     {'id': '43ad8ff4d3', 'name': '15129 Z6, Kaffeevollautomat', 'category_id': '857ffc6da1', 'manufacturer_id': 'jujhg2ra', 'image': 'assets/images/jura.png'},
        #     {'id': 'da6d43455f', 'name': 'LG 75UH780V, 190 cm (75 Zoll), UHD 4K', 'category_id': '857ffc6da1', 'manufacturer_id': 'jh3lkj32', 'image': 'assets/images/lg-tv.png'},
        #     {'id': 'debd77992a', 'name': 'HEISENBERG 1013021025 XD1 URBAN', 'category_id': 'c4e3185827', 'manufacturer_id': 'chd73bg7', 'image': 'assets/images/hnf.png'},
        #     {'id': '00acf20235', 'name': 'Custom Shop Solid Formed 17" Hollowbody Venetian - E-Gitarre', 'category_id': 'd8cd2a2a8e', 'manufacturer_id': '43mjhb', 'image': 'assets/images/egitarre.jpg'},
        #     ]

        # self.jsonFileHandler.storeData('dbProducts.json', products)
        productsFromFile = self.jsonFileHandler.getData('dbProducts.json')

        # check for filter and apply filter
        filterManufacturerId = requestData.get('requestArgs', {}).get('filter', {}).get('manufacturer_id', '')
        filterCategoryId = requestData.get('requestArgs', {}).get('filter', {}).get('category_id', '')

        if filterManufacturerId and filterCategoryId:
            filteredProducts = list(filter(lambda d: d['manufacturer_id'] == filterManufacturerId and d['category_id'] == filterCategoryId, productsFromFile))
        elif filterManufacturerId:
            filteredProducts = list(filter(lambda d: d['manufacturer_id'] == filterManufacturerId, productsFromFile))
        elif filterCategoryId:
            filteredProducts = list(filter(lambda d: d['category_id'] == filterCategoryId, productsFromFile))
        else:
            filteredProducts = productsFromFile
        responseService['response'] = {'product': filteredProducts}
        return responseService
