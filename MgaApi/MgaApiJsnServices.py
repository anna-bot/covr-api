#! /usr/bin/python
# -*- coding: utf-8; -*-
"""JsnService"""
import logging
import json
import base64
from uuid import uuid4
import types

from twisted.internet import defer
from twisted.web import http
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET
from TwistedComponents.Tools import IMainUtils
from AuthoriseAccess import AuthoriseAccess


SUPPORTED_METHODS_JSON = [
    'authenticate_register_register',
    'authenticate_login_login',
    'authenticate_refresh_refresh',
    'belonging_get',
    'belonging_overview_get',
    'belonging_overview_add',
    'belonging_overview_delete',
    'belonging_overview_update',
    'belonging_summary_get',
    'category_get',
    'insurance_contract_get',
    'insurance_contract_cancel',
    'insurance_contract_update',
    'insurance_coverage_get',
    'insurance_firm_get',
    'insurance_offer_get',
    'insurance_offer_close',
    'insurance_offer_overview_get',
    'manufacturer_get',
    'product_get',
    'account_get',
    'address_get',
    'address_country_get',
    'address_state_get',
    ]

SUPPORTED_GRANTED_APPLICATION_ACCESS_HASHES = [
    'fcb10d7f8479c0bd1852146e8c649a4d483cf65f39a4e62f4552419e47d3afb1',  # production
    '5tplkv47bexg08akyzesrm4hbhg3yibmj84cjxs8fkxekn8z6jtwzn7vjorb2ajv9i1w54jesbc4irzh'  # external developer
    ]


class MgaApiJsnServices(Resource):

    def getChild(self, path, request):
        LOG = logging.getLogger(__name__)
        LOG.debug('called MgaApiJsnServices.getChild')
        return self

    def render_HEAD(self, request):
        """DEFAULT handling for HEAD request: set method not allowed as http response"""
        d = defer.Deferred()
        d.addCallback(self.setMethodNotSupportedError)
        d.addCallback(self.finishRequest)
        d.callback(request)
        return NOT_DONE_YET

    def render_GET(self, request):
        """DEFAULT handling for GET request: set method not allowed as http response"""
        d = defer.Deferred()
        d.addCallback(self.printRequest)
        d.addCallback(self.maintenanceCheck)
        d.addCallback(self.finishRequest)
        d.callback(request)
        return NOT_DONE_YET

    def render_POST(self, request):
        """Default handling for POST request"""
        LOG = logging.getLogger(__name__)
        LOG.debug('called MgaApiJsnServices.render_POST')
        d = defer.Deferred()
        d.addCallback(self.printRequest)
        d.addCallback(self.getServiceData)
        d.addCallback(self.finishRequest)
        d.callback(request)
        return NOT_DONE_YET

    def finishRequest(self, request):
        request.finish()

    def maintenanceCheck(self, request):
        LOG = logging.getLogger(__name__)
        LOG.debug('called MgaApiJsnServices.maintenanceCheck')

        methodGet = request.path.strip('/')
        if methodGet == 'maintenancePing':
            try:
                dataPayload = str(request.args['payload'][0])
            except:
                dataPayload = 'pong'
            resultMaintenacePing = self.maintenancePing(dataPayload)
            request.write(json.dumps(resultMaintenacePing))
            return request
        else:
            return self.setMethodNotSupportedError(request)
        return request

    def maintenancePing(self, payload=''):
        """Return all passed arguments"""
        message = "Server echoed: "+str(payload)
        pingResponse = {'status': 'SUCCESS', 'message': message}
        return pingResponse

    def printRequest(self, request):
        LOG = logging.getLogger(__name__)
        LOG.debug('called MgaApiJsnServices.printRequest')
        debugClientDataString = 'CLIENT DATA IP: %s | method: %s | URI: %s | path: %s | args: %s | requestHeaders: %s' % (str(self.requestGetClientIp(request)), str(request.method), str(request.uri), str(request.path), str(request.args), str(request.requestHeaders))
        LOG.info(debugClientDataString)
        # LOG.info('------- client data --------')
        # LOG.info(' IP: %s' % str(request.getClientIP()))
        # LOG.info(' method: %s' % str(request.method))
        # LOG.info(' URI: %s' % str(request.uri))
        # LOG.info(' path: %s' % str(request.path))
        # LOG.info(' args: %s' % str(request.args))
        # LOG.info(' requestHeaders: %s' % str(request.requestHeaders))
        # LOG.info('-----------------------------')
        return request

    @defer.inlineCallbacks
    def getServiceData(self, request):
        LOG = logging.getLogger(__name__)

        authoriseAccess = AuthoriseAccess()
        autorisationCheckResponse = authoriseAccess.authoriseFromRequestHeader(request, SUPPORTED_GRANTED_APPLICATION_ACCESS_HASHES)
        if not autorisationCheckResponse.get('status', '') == 'SUCCESS':
            request.setResponseCode(http.UNAUTHORIZED)
            defer.returnValue(request)

        requestParameters = request.args.get('request', [])
        LOG.info(requestParameters)
        serviceMethodMain = ''
        serviceMethodSub = ''
        # version with 'request' key

        if requestParameters:
            LOG.info('****** part for using content of key request')
            try:
                LOG.info('---- DEBUG step2 requestparams[0] ------')
                rqParams = requestParameters[0]
                LOG.info(rqParams)
                LOG.info('---- DEBUG step3 base64 decode request ------')
                dataDecodedBase64 = self.decodeBase64(rqParams)
                LOG.info(dataDecodedBase64)
                LOG.info('---- DEBUG step4 json decode ------')
                requestDataJson = json.loads(dataDecodedBase64)
                LOG.info(requestDataJson)
                # try to use path of URI
                LOG.info('---- DEBUG step5.1 pathUri ------')
                pathUri = request.path.strip('/')  # get path data from URI
                LOG.info(pathUri)
                pathUriDictionary = pathUri.split('/')
                if pathUriDictionary:
                    LOG.info('---- DEBUG step5.3 pathUriDictionary validate ------')
                    pathUriDictionary = self.validatePath(pathUriDictionary)
                    serviceMethodMain = '_'.join(pathUriDictionary)
            except:
                errorMessage = {'status': 'ERROR', 'message': 'Failed to load json data from request', 'code': ''}
                LOG.error(errorMessage)
                request.write(json.dumps(errorMessage))
                defer.returnValue(request)
        else:
            LOG.info('****** part for using content flat structure')
            try:
                # temporary version for flat structure
                requestParametersFlat = request.args
                LOG.info('---- DEBUG step2 requestparams flat ------')
                LOG.info(requestParametersFlat)
                LOG.info('---- DEBUG step3 base64 decode request ------')
                dataDecodedBase64 = self.decodeBase64(requestParametersFlat)
                LOG.info(dataDecodedBase64)
                LOG.info('---- DEBUG step4 json decode ------')
                try:
                    requestDataJson = json.loads(dataDecodedBase64)
                except:
                    LOG.info('ERROR decode of Json data failed, try using given structure')
                    # LOG.info(requestParametersFlat)
                    requestDataJson = requestParametersFlat
                LOG.info(requestDataJson)
                # LOG.info('calling transformFlatToStructured')
                requestDataJson = self.transformFlatToStructured(requestDataJson)
                LOG.info('---- DEBUG after transformFlatToStructure ------')
                LOG.info(requestDataJson)

                pathUri = request.path.strip('/')  # get path data from URI
                # LOG.info(pathUri)
                # LOG.info('---- DEBUG step5.2 pathUriDictionary ------')
                pathUriDictionary = pathUri.split('/')
                # LOG.info(pathUriDictionary)
                if pathUriDictionary:
                    # LOG.info('---- DEBUG step5.3 pathUriDictionary validate ------')
                    pathUriDictionary = self.validatePath(pathUriDictionary)
                    # LOG.info(pathUriDictionary)
                    # LOG.info('---- DEBUG step5.4 serviceMethodMain ------')
                    serviceMethodMain = '_'.join(pathUriDictionary)
                    # LOG.info(serviceMethodMain)
            except:
                errorMessage = {'status': 'ERROR', 'message': 'Failed to load json data from request', 'code': ''}
                LOG.error(errorMessage)
                request.write(json.dumps(errorMessage))
                defer.returnValue(request)

        serviceMethodSub = requestDataJson.get('service', {}).get('method', '')

        serviceMethod = "%s_%s" % (serviceMethodMain, serviceMethodSub)
        LOG.info('service method: %s' % (str(serviceMethod)))
        # short validation of request content
        # LOG.info(SUPPORTED_METHODS_JSON)
        if serviceMethod not in SUPPORTED_METHODS_JSON:
            errorMessage = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
            LOG.error(errorMessage)
            request.write(json.dumps(errorMessage))
            defer.returnValue(request)

        apiSession = self.buildApiSession()
        if 'api_session' not in requestDataJson:
            requestDataJson['api_session'] = apiSession
        requestData = {
            'requestArgs': requestDataJson,
            'requestPathSection': serviceMethod,
            'requestOriginalClientIP': self.requestGetClientIp(request),
            'requestOriginalArgs':  str(request.args),
            'requestOriginalHeaders': str(request.requestHeaders),
            'requestOriginalURI': str(request.uri),
            'method': serviceMethod,
            'api_session': apiSession
        }
        LOG.info('***** maik requestData *****')
        LOG.info(requestData)

        if serviceMethod in ['authenticate_register_register']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceAuthenticateRegister(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['authenticate_login_login']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceAuthenticateLogin(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['authenticate_refresh_refresh']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceAuthenticateRefresh(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['belonging_get']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceBelonging(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['belonging_overview_get', 'belonging_overview_add', 'belonging_overview_delete' ,'belonging_overview_update']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceBelongingOverview(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['belonging_summary_get']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceBelongingSummary(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info(resultsFromService)
        elif serviceMethod in ['category_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceCategory(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['insurance_contract_get', 'insurance_contract_cancel', 'insurance_contract_update']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceInsuranceContract(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['insurance_coverage_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceInsuranceCoverage(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['insurance_firm_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceInsuranceFirm(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['insurance_offer_get', 'insurance_offer_close']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceInsuranceOffer(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['insurance_offer_overview_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceInsuranceOfferOverview(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['manufacturer_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceManufacturer(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['product_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceProduct(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['account_get']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceAccount(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['address_get']:
            if self.securityTokenIsValid(requestData):
                resultsFromService = yield IMainUtils('mgaApi').marshallServiceAddress(serviceMethodMain, serviceMethodSub, requestData)
            else:
                request.setResponseCode(http.UNAUTHORIZED)
                resultsFromService = {'status': 'ERROR', 'message': 'Unauthorized', 'code': ''}
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['address_country_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceAddressCountry(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        elif serviceMethod in ['address_state_get']:
            resultsFromService = yield IMainUtils('mgaApi').marshallServiceAddressState(serviceMethodMain, serviceMethodSub, requestData)
            LOG.info('***** maik resultsFromService *****')
            LOG.info(resultsFromService)
        else:
            resultsFromService = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        request.write(json.dumps(resultsFromService))
        defer.returnValue(request)

    def securityTokenIsValid(self, requestData):
        LOG = logging.getLogger(__name__)
        validState = True
        authCustomerDataRequest = requestData.get('requestArgs', {}).get('auth', {})
        authSecurityToken = authCustomerDataRequest.get('security_token', '')
        if authSecurityToken.strip() == '':
            validState = False
            LOG.info('No security token existing')
        return validState

    def validatePath(self, pathData):
        # remove emtpy string '' before generating
        pathData = list(filter(None, pathData))
        return pathData

    def setMethodNotSupportedError(self, request):
        request.setResponseCode(http.NOT_ALLOWED)
        return request

    def decodeBase64(self, data):
        """decode base64 content. if content is not base64, return original data"""
        if '{' in data or '[' in data:
            # base 64 content will not have mentioned special chars inside
            return data
        try:
            return base64.decodestring(data)
        except Exception, e:
            # print 'exception occured during base64 decode: %s' % (str(e))
            pass
        return data

    def requestGetClientIp(self, request):
        """Get the client's IP address from the ``'X-Forwarded-For:'``
        header, or from general twisted resource
        @request: A ``Request`` for a :api:`twisted.web.resource.Resource`.
        @returns: The clients IP address if it was obtainable
        """
        # check for x-forwarded-for
        forwardedForHeader = request.getHeader("x-forwarded-for")
        if forwardedForHeader:
            clientIp = forwardedForHeader.split(",")[-1].strip()
            if not clientIp:
                clientIp = request.getClientIP()
        else:
            clientIp = request.getClientIP()
        return clientIp

    def buildApiSession(self):
        sessionid = "ApiMga%s" % uuid4().get_hex()[:16].upper()
        return sessionid

    def transformFlatToStructured(self, data):
        # LOG = logging.getLogger(__name__)
        # LOG.info('------- transformFlatToStructured -------')
        transFormedStructure = {}
        for flatDataKey, flatDataValue in data.iteritems():
            parts = flatDataKey.split('__')
            # LOG.info(parts)
            branch = transFormedStructure
            # print '-------------------'
            # print '*** parts: ', parts
            # print parts
            # print flatDataKey
            # print flatDataValue
            # print '-------------------'
            elementCount = len(parts)
            # print 'elemetcount: ', str(elementCount)
            for i, part in enumerate(parts):
                # print '**** part: ', part
                # print '*** partcounter: ', str(i)
                if i+1 == elementCount:
                    # last element
                    # test for elemet type
                    if isinstance(flatDataValue, types.ListType) and len(flatDataValue) == 1:
                        branch[part] = flatDataValue[0]
                    else:
                        branch[part] = flatDataValue
                else:
                    branch = branch.setdefault(part, {})
        return transFormedStructure
