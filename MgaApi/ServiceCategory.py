#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging
from JsonFileHandler import JsonFileHandler


class ServiceCategory(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceCategory.__init__")
        # self.mgaDatabase = dbDict['mgaDb']
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceCategory.handleService")
        print "called ServiceCategory.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceCategory.get")
        print "called ServiceCategory.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        # categories = [
        #     {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #     {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #     {'id': '857ffc6da1', 'parent_id': '', 'name': 'Haushalt', 'icon': 'home'},
        #     {'id': 'c4e3185827', 'parent_id': '', 'name': 'Fahrrad', 'icon': 'bicycle'},
        #     {'id': 'd8cd2a2a8e', 'parent_id': '', 'name': 'Musik', 'icon': 'headset'},
        #     {'id': 'c78dcb36dc', 'parent_id': 'd8cd2a2a8e', 'name': 'Handy', 'icon': 'phone-portrait'},
        #     ]

        categoriesFromFile = self.jsonFileHandler.getData('dbCategory.json')
        responseService['response'] = {'category': categoriesFromFile}

        return responseService
