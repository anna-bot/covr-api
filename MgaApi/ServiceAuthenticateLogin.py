#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Handler Authenticate Login"""
import logging
import mx.DateTime
from Validator import GeneralValidator


class ServiceAuthenticateLogin(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceAuthenticateLogin.__init__")
        print "called ServiceAuthenticateLogin.__init__"
        self.generalValidator = GeneralValidator()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceAuthenticateLogin.handleService")
        print "called ServiceAuthenticateLogin.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceLogin(self, requestData):
        self.LOG.info("called ServiceAuthenticateLogin.serviceLogin")
        print "called ServiceAuthenticateLogin.serviceLogin"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}

        # check for fake data only (plain username and passwort check)

        authCustomerDataRequest = requestData.get('requestArgs', {}).get('items', {})
        self.LOG.info("******** auth customer data request ********")
        self.LOG.info(authCustomerDataRequest)
        self.LOG.info("**********************************************************")

        authUsername = authCustomerDataRequest.get('user_name', '')
        authPassword = authCustomerDataRequest.get('password', '')

        if not self.generalValidator.isUsername(authUsername):
            responseService = {'status': 'ERROR', 'message': 'Invalid user name format', 'code': ''}
            return responseService
        if not self.generalValidator.isPassword(authPassword):
            responseService = {'status': 'ERROR', 'message': 'Invalid password format', 'code': ''}
            return responseService

        # do fake check for plain data
        if authUsername != 'hschulz' or authPassword != 'test123':
            responseService = {'status': 'ERROR', 'message': 'Username or password wrong', 'code': ''}
            return responseService

        # set fake response
        # get identifier and security token
        authResponse = {
            # 'identifier': 'e3c71eb22caf4dfe7f04704779a725f683f615129331ffd22a0c208084817c3d',
            'security_token': '2629ea0967e84075a2580b0ebdbf859f',
            'expire': self.createTokenExpireTime()
            }
        responseService['response'] = {'auth': authResponse}
        return responseService

    def createTokenExpireTime(self):
        """creates timestamp when token expires"""
        maxTokenAge = mx.DateTime.oneHour * 1  # 1 hour
        return (mx.DateTime.now()+maxTokenAge).strftime('%Y-%m-%dT%H:%M:%S')
