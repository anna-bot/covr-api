#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging


class ServiceBelonging(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceBelonging.__init__")
        print "called ServiceBelonging.__init__"

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceBelonging.handleService")
        print "called ServiceBelonging.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceBelonging.get")
        print "called ServiceBelonging.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        belongings = [
            {'id': '4ed1da869f', 'product_id': 'c789aba5ec', 'purchase_price': 800.00, 'purchase_date': '2017-01-16', 'receipt_file': ''}, # CANON EOS 700D Spiegelreflexkamera
            {'id': '6c1e554a6a', 'product_id': 'cda0f967cc', 'purchase_price': 1200.00, 'purchase_date': '2017-02-04', 'receipt_file': ''}, # SAMSUNG Galaxy S6 edge
            {'id': '0cd49c08af', 'product_id': '43ad8ff4d3', 'purchase_price': 2000.00, 'purchase_date': '', 'receipt_file': ''}, # JURA 15129 Z6, Kaffeevollautomat
            {'id': '163a40d137', 'product_id': '2638b0ef9a', 'purchase_price': 1200.00, 'purchase_date': '', 'receipt_file': ''}, #MICROSOFT Surface Pro 4
            {'id': '4d4adaceec', 'product_id': 'debd77992a', 'purchase_price': 3600.00, 'purchase_date': '2017-02-05', 'receipt_file': ''}, # HNF HEISENBERG 1013021025 XD1 URBAN
            {'id': '57129c5005', 'product_id': '3f6a6a5ca8', 'purchase_price': 2999.00, 'purchase_date': '2017-02-04', 'receipt_file': ''}, #SONY SEL-70200GM 70 mm-200 mm f/2.8
            {'id': '90a426f1e0', 'product_id': 'd83919ffa5', 'purchase_price': 1269.00, 'purchase_date': '2017-02-04', 'receipt_file': ''}, #APPLE iPad Pro WiFi + Cellular, Tablet mit 12.9 Zoll, 256 GB Speicher, iOS 9, Silber
            {'id': '97f40fb633', 'product_id': 'da6d43455f', 'purchase_price': 4499.00, 'purchase_date': '', 'receipt_file': ''}, #LG 75UH780V, 190 cm (75 Zoll), UHD 4K
            {'id': 'd2f91c3ec9', 'product_id': 'c434413af4', 'purchase_price': 3199.00, 'purchase_date': '', 'receipt_file': ''}, # APPLE MacBook Pro mit Touch Bar und Touch ID, Notebook mit 15.4 Zoll Display
            {'id': '5b95f8e3cf', 'product_id': '0edf89090f', 'purchase_price': 1229.00, 'purchase_date': '2017-01-24', 'receipt_file': ''}, # CANON EF 8-15mm f/4L Fisheye USM 8 mm-15 mm Objektiv f/4
            {'id': '10ebf35d98', 'product_id': '00acf20235', 'purchase_price': 6099.00, 'purchase_date': '', 'receipt_file': ''}, # Gibson Custom Shop Solid Formed 17" Hollowbody Venetian - E-Gitarre
            {'id': 'e84ec443d3', 'product_id': 'ad30258748', 'purchase_price': 3949.00, 'purchase_date': '2017-01-24', 'receipt_file': ''}, # Sonor ProLite PL 12 Studio1 Natural - Schlagzeug
            {'id': '2390e0a09e', 'product_id': '8ed4e02ced', 'purchase_price': 6990.00, 'purchase_date': '', 'receipt_file': ''} #Trautwein Modell TRADITION II Chrom - Schwarz poliert
        ]
        responseService['response'] = {'belonging': belongings}

        return responseService
