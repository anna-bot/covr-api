#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Belonging Summary"""
import logging
from JsonFileHandler import JsonFileHandler


class ServiceBelongingSummary(object):
    """This service has as aim get all the belongings from the user, sum them up by category and deliver this to the Einsr app.
    This function has to offer only read requests. No Insert, Update or Delete requests are necessary."""

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceBelongingSummary.__init__")
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceBelongingSummary.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceBelongingSummary.get")
        # self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        # belongingsOverviewItems = [
        #     {
        #         'id': '4ed1da869f',
        #         'manufacturer': {'id': 'j23h3', 'name': 'Canon', 'logo_image': 'assets/logos/canon.jpg', 'lang': 'de-DE' },
        #         'name': 'CANON EOS 700D Spiegelreflexkamera',
        #         'category': {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #         'purchase_price': 800.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 10.00,
        #         'image': 'assets/images/canon.png'},
        #     {
        #         'id': '6c1e554a6a',
        #         'manufacturer': {'id': 'hg23jh', 'name': 'Samsung', 'logo_image': 'assets/logos/samsung.jpg', 'lang': 'de-DE' },
        #         'name': 'SAMSUNG Galaxy S6 edge',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 1200.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 17.55,
        #         'image': 'assets/images/galaxy.png'},
        #     {
        #         'id': '0cd49c08af',
        #         'manufacturer': {'id': 'jujhg2ra', 'name': 'JURA', 'logo_image': 'assets/logos/jura.png', 'lang': 'de-DE' },
        #         'name': 'JURA 15129 Z6, Kaffeevollautomat',
        #         'category': {'id': '857ffc6da1', 'parent_id': '', 'name': 'Haushalt', 'icon': 'home'},
        #         'purchase_price': 2000.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 12.13,
        #         'image': 'assets/images/jura.png'},
        #     {
        #         'id': '163a40d137',
        #         'manufacturer': {'id': 'juikpms', 'name': 'Microsoft', 'logo_image': 'assets/logos/microsoft.jpg', 'lang': 'de-DE' },
        #         'name': 'MICROSOFT Surface Pro 4',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 1200.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 5.50,
        #         'image': 'assets/images/surface.png'},
        #     {
        #         'id': '57129c5005',
        #         'manufacturer': {'id': 'jh2g3', 'name': 'Sony', 'logo_image': 'assets/logos/sony.svg', 'lang': 'de-DE' },
        #         'name': 'SONY SEL-70200GM 70 mm-200 mm f/2.8',
        #         'category': {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #         'purchase_price': 2999.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 5.90,
        #         'image': 'assets/images/objective.png'},
        #     {
        #         'id': '90a426f1e0','manufacturer': {'id': '62fhv2', 'name': 'Apple', 'logo_image': 'assets/logos/apple.png', 'lang': 'de-DE' },
        #         'name': 'APPLE iPad Pro WiFi + Cellular, Tablet mit 12.9 Zoll, 256 GB Speicher, iOS 9, Silber',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 1269.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 10.45,
        #         'image': 'assets/images/ipad.png'},
        #     {
        #         'id': '97f40fb633',
        #         'manufacturer': {'id': 'jh3lkj32', 'name': 'LG', 'logo_image': 'assets/logos/lg.jpg', 'lang': 'de-DE' },
        #         'name': 'LG 75UH780V, 190 cm (75 Zoll), UHD 4K',
        #         'category': {'id': '857ffc6da1', 'parent_id': '', 'name': 'Haushalt', 'icon': 'home'},
        #         'purchase_price': 4499.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 30.10,
        #         'image': 'assets/images/lg-tv.png'},
        #     {
        #         'id': 'd2f91c3ec9',
        #         'manufacturer': {'id': '62fhv2', 'name': 'Apple', 'logo_image': 'assets/logos/apple.png', 'lang': 'de-DE' },
        #         'name': 'MacBook Pro mit Touch Bar und Touch ID, Notebook mit 15.4 Zoll Display',
        #         'category': {'id': 'ed2aaa52b4', 'parent_id': '', 'name': 'Elektronik', 'icon': 'laptop'},
        #         'purchase_price': 3199.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 7.90,
        #         'image': 'assets/images/macbook.png'},
        #     {
        #         'id': '5b95f8e3cf',
        #         'manufacturer': {'id': 'j23h3', 'name': 'Canon', 'logo_image': 'assets/logos/canon.jpg', 'lang': 'de-DE' },
        #         'name': 'EF 8-15mm f/4L Fisheye USM 8 mm-15 mm Objektiv f/4',
        #         'category': {'id': 'cb68c91a41', 'parent_id': '', 'name': 'Kamera', 'icon': 'camera'},
        #         'purchase_price': 1229.00,
        #         'is_insured': True,
        #         'lowest_insurance_offer': 15.50,
        #         'image': 'assets/images/objective-fisheye.png'},
        #     {
        #         'id': '10ebf35d98',
        #         'manufacturer': {'id': '43mjhb', 'name': 'Gibson', 'logo_image': 'assets/logos/gibson.png', 'lang': 'de-DE' },
        #         'name': 'Custom Shop Solid Formed 17 Hollowbody Venetian - E-Gitarre',
        #         'category': {'id': 'd8cd2a2a8e', 'parent_id': '', 'name': 'Musik', 'icon': 'headset'},
        #         'purchase_price': 6099.00,
        #         'is_insured': False,
        #         'lowest_insurance_offer': 5.99,
        #         'image': 'assets/images/egitarre.jpg'}
        # ]

        # self.jsonFileHandler.storeData('dbBelongingOverview.json', belongingsOverviewItems)

        belongingOverviewFromFile = self.jsonFileHandler.getData('dbBelongingOverview.json')

        # This service has as aim get all the belongings from the user, sum them up by category and deliver this to the Einsr app. This function has to offer only read requests. No Insert, Update or Delete requests are necessary.
        # summarised
        categoryDetails = {}

        for belonging in belongingOverviewFromFile:
            # current belonging detail
            categoryId = belonging.get('category', '').get('id', '')
            categoryParentId = belonging.get('category', '').get('parent_id', '')
            categoryName = belonging.get('category', '').get('name', '')
            categoryIcon = belonging.get('category', '').get('icon', '')
            currentTotalValue = float(belonging.get('purchase_price', 0.0))
            currentTotalValueInsured = float(belonging.get('purchase_price', 0.0)) if belonging.get('is_insured', False) else 0.0
            currentElementCount = 1
            if categoryId not in categoryDetails:
                categoryDetails[categoryId] = {'id': categoryId,
                                               'parent_id': categoryParentId,
                                               'name': categoryName,
                                               'icon': categoryIcon,
                                               'total_value': currentTotalValue,
                                               'total_value_insured': currentTotalValueInsured,
                                               'count': currentElementCount}
            else:
                categoryDetails[categoryId]['total_value'] = categoryDetails[categoryId]['total_value'] + currentTotalValue
                categoryDetails[categoryId]['total_value_insured'] = categoryDetails[categoryId]['total_value_insured'] + currentTotalValueInsured
                categoryDetails[categoryId]['count'] = categoryDetails[categoryId]['count'] + currentElementCount

        belongingSummary = list(element for element in categoryDetails.values())

        responseService['response'] = {'belonging_summary': belongingSummary}
        return responseService
