#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Service Manufacture"""
import logging
from JsonFileHandler import JsonFileHandler


class ServiceManufacturer(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceManufacturer.__init__")
        self.jsonFileHandler = JsonFileHandler()

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceManufacturer.handleService")
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceManufacturer.get")
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        # manufacturer = [
        #     {'id': '62fhv2', 'name': 'Apple', 'logo_image': 'assets/logos/apple.png', 'lang': 'de-DE'},
        #     {'id': 'hg23jh', 'name': 'Samsung', 'logo_image': 'assets/logos/samsung.jpg', 'lang': 'de-DE'},
        #     {'id': 'jujhg2ra', 'name': 'JURA', 'logo_image': 'assets/logos/jura.png', 'lang': 'de-DE'},
        #     {'id': 'juikpms', 'name': 'Microsoft', 'logo_image': 'assets/logos/microsoft.jpg', 'lang': 'de-DE'},
        #     {'id': 'jh2g3', 'name': 'Sony', 'logo_image': 'assets/logos/sony.svg', 'lang': 'de-DE'},
        #     {'id': 'jh3lkj32', 'name': 'LG', 'logo_image': 'assets/logos/lg.jpg', 'lang': 'de-DE'},
        #     {'id': 'j23h3', 'name': 'Canon', 'logo_image': 'assets/logos/canon.jpg', 'lang': 'de-DE'},
        #     {'id': '43mjhb', 'name': 'Gibson', 'logo_image': 'assets/logos/gibson.png', 'lang': 'de-DE'},
        #     {'id': 'chd73bg7', 'name': 'HNF Heisenberg', 'logo_image': '', 'lang': 'de-DE'}
        # ]
        # self.jsonFileHandler.storeData('dbManufacturer.json', manufacturer)

        manufacturerFromFile = self.jsonFileHandler.getData('dbManufacturer.json')

        # filter functionality:
        # get manufacturer (unique) by category
        # - will search for category in products and then get the manufactures details from manufatures json file

        # check for filter and apply filter
        filterCategoryId = requestData.get('requestArgs', {}).get('filter', {}).get('category_id', '')

        manufactureIdsList = []
        if filterCategoryId:
            # get unique manufacturer_id based on products
            productsFromFile = self.jsonFileHandler.getData('dbProducts.json')
            self.LOG.info(productsFromFile)
            for product in productsFromFile:
                if product['category_id'] == filterCategoryId:
                    manufactureIdsList.append(product['manufacturer_id'])

            # uiqnue list of manufacture ids
            manufactureIdsList = list(set(manufactureIdsList))

            if manufactureIdsList:
                filteredManufacturer = list(filter(lambda d: d['id'] in manufactureIdsList, manufacturerFromFile))
            else:
                filteredManufacturer = []
        else:
            filteredManufacturer = manufacturerFromFile

        responseService['response'] = {'manufacturer': filteredManufacturer}
        return responseService
