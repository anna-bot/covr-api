#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Cleaning Manager
This is a little class for managing stuff at the start and stop
of a Twisted Application.
Currently this Class only removes the PIDFILE
"""
__version__ = "$Revision: 1.100 $"[11:-2]

import twisted
from os import remove
from os.path import isfile

try:
    twistedVersion = [int(i) for i in twisted.__version__.split('.')[:2]]
except:
    from twisted import copyright
    twistedVersion = [int(i) for i in copyright.version.split('.')[:2]]


class CleaningManager:
    """If the service get stopped by a signal or something diffrent
    the stopService method of this class is called and cleans all
    necessary stuff.
    """
    def __init__(self, PIDFILE):
        """Save the PIDFILE locally and install the handler
        """
        self.PIDFILE = PIDFILE

    def sigTerm(self):
        """Give a errorrmessage and delete the PIDFILE
        """
        print 'Cleaning in progress ...'
        if isfile(self.PIDFILE):
            print 'Remove PIDFILE (%s)' % (self.PIDFILE)
            remove(self.PIDFILE)
        print '.. application is clean.'

    def stopService(self):
        self.sigTerm()


if (twistedVersion[0] == 2 and twistedVersion[1] >= 5)  or \
   twistedVersion[0] > 2:
    pass
else:
    from twisted.internet.app import ApplicationService
    CleaningManager.__bases__ = (ApplicationService,)
