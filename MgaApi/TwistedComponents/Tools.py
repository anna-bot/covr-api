#! /usr/bin/python
# -*- coding: utf-8; -*-

import os

from types import StringTypes
from weakref import WeakValueDictionary

from zope.interface.interface import adapter_hooks
from zope.interface import Interface, Attribute
from zope.interface.exceptions import BrokenImplementation
from zope.interface.verify import verifyObject

from AttrDict import AttrDict


class AttrWeakValueDictionary(AttrDict, WeakValueDictionary):
    """Make WeakValueDictionary also attribute lookup-able"""

    def __init__(self, *args, **kwargs):
        AttrDict.__init__(self, *args, **kwargs)
        WeakValueDictionary.__init__(self, *args, **kwargs)


class IUtility(Interface):
    """Because zope.components is not a default installation we can not use its implementation of utilitis.
    So an objekt which implements this interface takes a string containing the name for the component which should be looked up
    and will be returned.

    Given that its simpler to say: all strings which wants to be adapt to an implementer of IUtility get back the component looked up by name

    It's only a marker interface.
    """


class IMainUtils(IUtility):
    """Marker interface for register instances created in main() function"""


def register_names(dict_names_objs, interface=IUtility, only_weak=False, verify=True):
    """Register an adapter for strings to arbitary objects.

    @param dict_names_objs a dict with names as keys and instances as values
    @param interface the interface on which the name should be looked up
    @param only_weak only keeps references to objects which can be hold weakly
    @param verify verify that the object provides tentative the interface
    """

    try:
        dict_names_objs = AttrWeakValueDictionary(dict_names_objs)
        weak = dict_names_objs.keys()
    except TypeError:
        if only_weak:
            wd = AttrWeakValueDictionary()
            for key, value in dict_names_objs.iteritems():
                try:
                    wd[key] = value
                except TypeError:
                    pass
            weak = wd.keys()
            dict_names_objs = wd
        else:
            dict_names_objs = AttrDict(dict_names_objs)
            weak = False

    def adapt_names(iface, obj):
        if iface.isOrExtends(interface) and isinstance(obj, StringTypes):
            if verify:
                verifyObject(iface, dict_names_objs, tentative=True)
            return dict_names_objs.get(obj)

    adapter_hooks.append(adapt_names)
    return (weak, adapt_names)
