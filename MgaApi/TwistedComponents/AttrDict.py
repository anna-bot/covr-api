#! /usr/bin/python
# -*- coding: utf-8; -*-


class AttrDict(dict):
    """This Attribute dictionary class creates a dict whose items can also be accessed as member variables.

    >>> d = AttrDict(a=1, b=2)
    >>> d['c'] = 3
    >>> print d.a, d.b, d.c
    1 2 3
    >>> d.b = 10
    >>> print d['b']
    10
    """
    def __init__(self, *args, **kwargs):
        self.__dict__ = self
        super(AttrDict, self).__init__(*args, **kwargs)