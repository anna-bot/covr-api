#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api"""

import sys
import os

import argparse
import configobj

import json
import types
from twisted.internet import reactor
from twisted.web.resource import Resource
from twisted.web.server import Site

from TwistedComponents.CleaningManager import CleaningManager
from TwistedComponents.Tools import register_names, IMainUtils
from DbPoolFactory import DbPoolFactory

from MgaApiJsnServices import MgaApiJsnServices
from MgaApiCore import MgaApiCore
from GeneralSSLContextFactory import GeneralSSLContextFactory
from Mail import Mail

import logging
import LoggingConfigurationLoader

NAME = 'MgaApi'
CFG = '/home/magapi/mga-api/MgaApi/Configs/MgaApi.cfg'


def makeService():
    """Build the service"""
    logger = logging.getLogger(__name__)
    logger.debug('called makeService')

    # check parameter calls
    usage = "usage %prog [options]"
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument("-c", "--config", dest='configfile', default=CFG, help="configuration file", metavar="CONFIG")
    parser.add_argument("-d", "--debug", dest='debug', default=False, help="enables stdout")
    args = parser.parse_args()

    # config
    config = configobj.ConfigObj(args.configfile)
    if not config:
        sys.exit('ERROR: Configuration file is missing: %s' % (args.configfile))

    if isinstance(config['main']['connectionType'], types.StringType):
        config['main']['connectionType'] = [config['main']['connectionType']]

    configPidfile = config['main']['pidfile']
    configlogConfiguration = config['main']['logConfiguration']
    configConnectionType = config['main']['connectionType']
    configConnectionTCPPort = config['tcp']['port']
    configConnectionTCPInterface = config['tcp'].get('interface', 'localhost')
    configConnectionSSLPort = config['ssl']['port']
    configConnectionSSLInterface = config['ssl'].get('interface', 'localhost')
    configConnectionSSLKey = config['ssl']['key']
    configConnectionSSLCrt = config['ssl']['crt']

    cleaningManager = CleaningManager(configPidfile)

    # start logging
    if args.debug:  # set loglevel to DEBUG
        LoggingConfigurationLoader.setupLogging(default_path=configlogConfiguration, default_level=logging.DEBUG)
    else:
        LoggingConfigurationLoader.setupLogging(default_path=configlogConfiguration)

    # ##########################################################################
    # #                       Creating database-connections                    #
    # ##########################################################################
    dbDict = {}
    pgSqlPoolFactory = DbPoolFactory("PgSQL")
    dbDict['mgaDb'] = pgSqlPoolFactory.get(config['mgaDb'])
    dbDict['logsDb'] = pgSqlPoolFactory.get(config['logsDb'])

    logger.info("")
    logger.info("================== %s starting up  ============================" % (NAME))
    logger.info("... using pid file       : %s" % (configPidfile))
    logger.info("... using connection type: %s" % (str(configConnectionType)))

    # startup
    mgaApi = MgaApiCore(dbDict)

    # add mail to be accessible from anywhere
    mail = Mail(smtp='mail.policendirekt.de:2525', smtpUser='POLICENDIREKT\donotreply_cov', smtpPassword = 'Ikurimawo553')

    # # make local variables accessible over IMainUtils-iface
    # local = locals()
    iMainDict = {'config': config,
                 'dbDict': dbDict,
                 'mgaApi': mgaApi,
                 'mail': mail}
    register_names(iMainDict, IMainUtils)

    mgaApiJsnServices = MgaApiJsnServices()
    jsnServices = Site(mgaApiJsnServices)

    if 'SSL' in configConnectionType:
        generalSSLContextFactory = GeneralSSLContextFactory()
        sslContextFactory = generalSSLContextFactory.generateSSLContextFactory(configConnectionSSLKey, configConnectionSSLCrt)
        reactor.listenSSL(int(configConnectionSSLPort), jsnServices, sslContextFactory, interface=configConnectionSSLInterface)
        logger.info("Initialising %s JSON interface for SSL %s:%s" % (NAME, str(configConnectionSSLInterface), str(configConnectionSSLPort)))
    if 'TCP' in configConnectionType:
        reactor.listenTCP(int(configConnectionTCPPort), jsnServices, interface=configConnectionTCPInterface)
        logger.info("Initialising %s JSON interface for TCP %s:%s" % (NAME, str(configConnectionTCPInterface), str(configConnectionTCPPort)))

    logger.info("%s initialisation complete" % (NAME))
    logger.info("=============================================================================")
    logger.info("")

    reactor.addSystemEventTrigger("during", "shutdown", cleaningManager.stopService)
    reactor.run()
    logger.info("%s shutdown complete" % (NAME))

if __name__ == "__main__":
    makeService()
