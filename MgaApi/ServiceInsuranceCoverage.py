#! /usr/bin/python
# -*- coding: utf-8; -*-
"""Mga-Api Core"""
import logging


class ServiceInsuranceCoverage(object):

    def __init__(self, dbDict):
        self.LOG = logging.getLogger(__name__)
        self.LOG.info("called ServiceInsuranceCoverage.__init__")
        print "called ServiceInsuranceCoverage.__init__"

    def handleService(self, serviceParameter):
        self.LOG.info("called ServiceInsuranceCoverage.handleService")
        print "called ServiceInsuranceCoverage.handleService"
        self.LOG.info(serviceParameter)

        requestData = serviceParameter.get('requestData', {})
        serviceMethodSub = serviceParameter.get('serviceMethodSub', '').capitalize()
        formatFunctionName = 'service%s' % (serviceMethodSub)
        if hasattr(self.__class__, formatFunctionName) and callable(getattr(self.__class__, formatFunctionName)):
            functionName = getattr(self.__class__, formatFunctionName, None)
            serviceResponse = functionName(self, requestData)
        else:
            serviceResponse = {'status': 'ERROR', 'message': 'Method not supported', 'code': ''}
        return serviceResponse

    def serviceGet(self, requestData):
        self.LOG.info("called ServiceInsuranceCoverage.get")
        print "called ServiceInsuranceCoverage.get"
        self.LOG.info(requestData)

        responseService = {'status': 'SUCCESS', 'message': '', 'code': ''}
        coverage = [
            {'id': 'fe28138d3c', 'name': 'Sturz- und Bruchschäden'},
            {'id': '6fc15e22d4', 'name': 'Bedienungsfehler'},
            {'id': '3651eefc76', 'name': 'Wasserschäden'},
            {'id': 'a1daeb2372', 'name': 'Diebstahl'},
            {'id': 'a1573a99a1', 'name': 'Schäden durch Flüssigkeit'},
            {'id': '908c6ef30d', 'name': 'Kurzschluss und Überspannung'},
            {'id': '804e2a3d21', 'name': 'Material- und Konstruktionsfehler'},
        ]
        responseService['response'] = {'insurance_coverage': coverage}

        return responseService
