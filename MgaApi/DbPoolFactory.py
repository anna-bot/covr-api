#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generic database connection pool management for a variety of database servers.
Currently PostgreSQL is supported only.
"""

import psycopg2.extras
from twisted.enterprise import adbapi


class DbPoolFactory(object):
    __factories = ["PGSQL"]
    __factory = None

    def __init__(self, provider):
        if provider.upper() not in self.__factories:
            raise NameError("No database factory found for '%s'." % provider)
        else:
            if provider.upper() == "PGSQL":
                self.__factory = self.__PostgreSQLPoolFactory

    def get(self, dbConfig, **kwargs):
        return self.__factory(dbConfig, **kwargs)

    def __PostgreSQLPoolFactory(self, dbConfig, **kwargs):
        return adbapi.ConnectionPool(
            "psycopg2",
            host=str(dbConfig["host"]),
            database=str(dbConfig["name"]),
            user=str(dbConfig["user"]),
            password=str("pass" in dbConfig and dbConfig["pass"] or ""),
            cp_min=int("minconnect" in dbConfig and dbConfig["minconnect"] or 1),
            cp_max=int("maxconnect" in dbConfig and dbConfig["maxconnect"] or 1),
            **kwargs)
