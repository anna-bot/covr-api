# MGA API


# Communication
This document is the technical documentation for our API that is being used for the communication between APP and Covomo backend. Communication between APP and Covomo endpoint is exclusively performed via HTTPS POST. The request is carrying a JSON as payload and is structured as followed. Every request is being answered with a response (or HTTP error). Request has to be encoded in UTF-8.


## Environment
Covomo is providing one environment at the moment.

| Environment | URL                    |
| ----------- | ---------------------- |
| Production  | https://api.covomo.com |


## Endpoints
The API is providing the following endpoint.

| Endpoint                             | Description                                               |
| ------------------------------------ | --------------------------------------------------------- |
| /api_mga/product                     | product information (filter: category_id, manufacturer_id |
| /api_mga/category                    | list of categories                                        |
| /api_mga/manufacturer                | manufacturer information (filter: category_id)            |
| /api_mga/belonging                   | user belongings                                           |
| /api_mga/belonging/overview          | user belongings with detailed product, category,.. (method: add, update, delete, get)        |
| /api_mga/belonging/summary           | summary of user belongings per category                   |
| /api_mga/insurance/contract          | user contract data (method: get, cancel, update)          |
| /api_mga/insurance/firm              | list of insurance firm                                    |
| /api_mga/insurance/offer             | user insurance offer                                      |
| /api_mga/insurance/offer/overview    | user belongings with detailed coverage, price values,..   |
| /api_mga/authenticate/register       | controls user access - register the user                  |
| /api_mga/authenticate/login          | controls user access - log the user in                    |
| /api_mga/authenticate/refresh        | controls user access - refresh security token             |
| /api_mga/account                     | account/user general data                                 |
| /api_mga/address                     | client/user address                                       |
| /api_mga/address/country             | list of country                                           |
| /api_mga/address/state               | list of state (filter: country_id)                        |


## Authentication
Basic HTTP Authentication is used. Authentication string will be provided by Covomo for each customer/application.
Please acquire for your authentication string.
Each request must have HTTP Authorization header.

Type: POST
Header: Authorization: Basic "Base64 encoded identifier"
Header: Content-Type: application/x-www-form-urlencoded

Example:
String identifier example "fcb10d7f8479c0bd1852146e8c649a4d483cf65f39a4e62f4552419e47d3afb1" must be base64 encoded.
`Authorization: Basic ZmNiMTBkN2Y4NDc5YzBiZDE4NTIxNDZlOGM2NDlhNGQ0ODNjZjY1ZjM5YTRlNjJmNDU1MjQxOWU0N2QzYWZiMQ=='


## HTTP Status Codes
Following HTTP status codes are used:

| Code | Description                        |
| ---- | ---------------------------------- |
| 200  | Request was processed successfully |
| 401  | Authentication failed              |
| 404  | Not Found                          |
| 500  | Internal Server Error              |


# Endpoints


## Authenticate register


If there is no error during register the user with transmitted data, an security token is returned. This token expires in the amount of seconds specified in 'expire'. Within the scope of validity, the token can be refrehed/extended using `/authenticate/refresh`.


### Endpoint Specification

| Endpoint                       | Content-Type     | Encoding |
| ------------------------------ | ---------------- | -------- | 
| /api_mga/authenticate/register | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "register"
                  },
                  "items": {
                    "user_name" : "hschulz",
                    "password": "test123",
                    "email": "henrique.schulz@covomo.de",
                    "unit_identifier":""
                  }
                }
            ]
}
```



## Authenticate login

If there is no error during login the user with transmitted data, an security token is returned. This token expires in the amount of seconds specified in 'expire'. Within the scope of validity, the token can be refrehed/extended using `/authenticate/refresh`.


### Endpoint Specification

| Endpoint                    | Content-Type     | Encoding |
| --------------------------- | ---------------- | -------- | 
| /api_mga/authenticate/login | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "login"
                  },
                  "items": {
                    "user_name" : "hschulz",
                    "password": "test123",
                    "unit_identifier":""
                  }
                }

            ]
}
```



## Authenticate refresh

Extend the live time of currently used access token. The new remaining time is indicated in seconds again.
Changes to the token themselves are reserved. The transmitted value should therefore be accepted


### Endpoint Specification

| Endpoint                            | Content-Type     | Encoding |
| ----------------------------------- | ---------------- | -------- | 
| /api_mga/authenticate/refresh       | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f",
                    "unit_identifier": ""
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "refresh"
                  }
                }

            ]
}
```


## Belonging

### Endpoint Specification

| Endpoint              | Content-Type     | Encoding |
| --------------------- | ---------------- | -------- | 
| /api_mga/belonging    | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }

            ]
}
```


## Belonging overview

### Endpoint Specification

| Endpoint                    | Content-Type     | Encoding |
| --------------------------- | ---------------- | -------- | 
| /api_mga/belonging/overview | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }

            ]
}
```


## Belonging summary

### Endpoint Specification

| Endpoint                    | Content-Type     | Encoding |
| --------------------------- | ---------------- | -------- | 
| /api_mga/belonging/summary  | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }

            ]
}
```



## Category

### Endpoint Specification

| Endpoint              | Content-Type     | Encoding |
| --------------------- | ---------------- | -------- | 
| /api_mga/category     | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }
            ]
}
```



## Product

### Endpoint Specification

| Endpoint              | Content-Type     | Encoding |
| --------------------- | ---------------- | -------- |
| /api_mga/product      | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "filter": {
                    "category_id": "",
                    "manufacturer_id": ""
                  }
                }
            ]
}
```



## Manufacturer

### Endpoint Specification

| Endpoint              | Content-Type     | Encoding |
| --------------------- | ---------------- | -------- | 
| /api_mga/manufacturer | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "filter": {
                    "category_id": ""
                  }
                }
            ]
}
```


## Insurance contract

### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/insurance/contract     | application/json | utf-8    |


| Service Method | Description                                                   |
| -------------- | ------------------------------------------------------------- |
| get            | get contract by belonging id                                  |
| cancel         | cancel contract by contract id                                |
| update         | update value 'automatic_renewal' corresponding to contract id |

### Request 'get' example Json

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "filter": {
                    "belonging_id": ""
                  }
                }
            ]
}
```
### Request 'cancel' example Json

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "cancel"
                  },
                  "filter": {
                    "contract_id": ""
                  }
                }
            ]
}
```

### Request 'update' example Json

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "update"
                  },
                  "items": {
                    "contract_id": "",
                    "content" : {"automatic_renewal": "True"}
                  }
                }
            ]
}
```



## Insurance firm

### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/insurance/firm         | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }
            ]
}
```



## Insurance offer

### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/insurance/offer        | application/json | utf-8    |

| Service Method | Description          |
| -------------- | -------------------- |
| get            | create offer         |
| close          | close the offer      |

### Request 'get' example Json

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "filter": {
                    "belonging_id": "",
                    "purchase_price": ""
                  }
                }
            ]
}
```


### Request 'close' example Json

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "close"
                  },
                  "filter": {
                    "offer_id": "",
                    "billing_cycle_id": ""
                  }
                }
            ]
}
```

## Insurance offer overview

### Endpoint Specification

| Endpoint                          | Content-Type     | Encoding |
| --------------------------------- | ---------------- | -------- | 
| /api_mga/insurance/offer/overview | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }
            ]
}


## Account

### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/account                | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }
            ]
}
```



## Address

### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/address                | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "items": {
                    "id" : []
                  }
                }
            ]
}
```


## Address Country

### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/address/country        | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  }
                }
            ]
}
```


## Address State


### Endpoint Specification

| Endpoint                        | Content-Type     | Encoding |
| ------------------------------- | ---------------- | -------- | 
| /api_mga/address/state          | application/json | utf-8    |

### Request

# Example Json:

```
{"request": [
                {
                  "auth": {
                    "security_token": "2629ea0967e84075a2580b0ebdbf859f"
                  },
                  "general": {
                    "location":"de-DE"
                  },
                  "service": {
                    "method": "get"
                  },
                  "filter": {
                    "country_id" : "DE"
                  }
                }
            ]
}
```

