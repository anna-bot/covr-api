============
Installation
============

### install necessary system packages
apt-get install python2.7 python-dev python-pip python-setuptools libpq-dev


### install project depending packages via pip
pip install -r requirements.txt


### to run as user 'mgaapi' in group 'covomo'

# creating group covomo
groupadd covomo

# creating user mgaapi
useradd -m mgaapi

# adding user 'mgaapi' to group 'covomo'
addgroup mgaapi covomo


# create ssh key for the user
ssh-keygen

# copy ssh public key as deploy key to bitbucket
cat .ssh/id_rsa.pub   ........



###  checkout (clone) the repository

- checkout in /home/mgaapi/
  git clone git@bitbucket.org:covomo/mga-api.git

# change user rights for the checked out repo
- cd /home/mgaapi/
- chown -R mgaapi:covomo mga-api

- change logging config in Configs/Logging.json to have absolute path:
  eg for info_file_handler use: "filename": "/home/mgaapi/MgaApiInfos.log"
  eg for error_file_handler use: "filename": "/home/mgaapi/MgaApiErrors.log"

 - cd /home/mgaapi/mga-api/MgaApi/
 - start with:  nohup python MgaApi.py -c Configs/MgaApi.cfg >/dev/null 2>&1 &
