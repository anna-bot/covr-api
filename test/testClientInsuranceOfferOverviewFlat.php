<?php
// test client

$dataString = file_get_contents('Example_json/Flat_Service_with_user_lang_ger.json');
$postData = http_build_query(json_decode($dataString, true));
$opts = array(
   'http' => [
       'method'  => 'POST',
       'header'  => ['Content-type: application/x-www-form-urlencoded',  'Authorization: Basic '. base64_encode("fcb10d7f8479c0bd1852146e8c649a4d483cf65f39a4e62f4552419e47d3afb1")],
       'content' => $postData
   ],

   'ssl' => [
       'verify_peer' => false,
       'verify_peer_name' => false,
   ],
);

$context = stream_context_create($opts);

$url = 'http://127.0.0.1/api_mga/insurance/offer/overview';
//$url = 'https://api.covomo.com/api_mga/insurance/offer/overview';
echo "\n-------------------------------------";
echo "\nRequest: ".$url." (".$dataString.")\n";

$result =  file_get_contents($url, false, $context);
echo "\nResponse headers:";
echo print_r($http_response_header);
echo "\nResponse";
echo $result;

?>
