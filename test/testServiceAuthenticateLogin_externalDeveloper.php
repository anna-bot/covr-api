<?php
// test client

$dataString = file_get_contents('Example_json/Flat_Service_Authenticate_Login.json');
$postData = http_build_query(json_decode($dataString, true));
$opts = array(
   'http' => [
       'method'  => 'POST',
       'header'  => ['Content-type: application/x-www-form-urlencoded',  'Authorization: Basic '. base64_encode("5tplkv47bexg08akyzesrm4hbhg3yibmj84cjxs8fkxekn8z6jtwzn7vjorb2ajv9i1w54jesbc4irzh")],
       'content' => $postData
   ],

   'ssl' => [
       'verify_peer' => false,
       'verify_peer_name' => false,
   ],
);

$context = stream_context_create($opts);
$url = 'https://api.covomo.com/api_mga/authenticate/login';
echo "\n-------------------------------------";
echo "\nRequest: ".$url." (".$dataString.")\n";

$result =  file_get_contents($url, false, $context);
echo "\nResponse headers:";
echo print_r($http_response_header);
echo "\nResponse";
echo $result;

?>
